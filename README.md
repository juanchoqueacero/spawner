Spawner
=======

Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

[![pipeline status](https://gitlab.com/rwr/spawner/badges/3.0/pipeline.svg)](https://gitlab.com/rwr/spawner/commits/3.0)

README
======
[README](https://gitlab.com/rwr/spawner/blob/master/README.md)

[Bukkit plugin page](https://dev.bukkit.org/projects/spawner)  
[Spigot plugin page](https://www.spigotmc.org/resources/spawner.38624/)  
[Source](https://gitlab.com/rwr/spawner)  
[Documentation](https://gitlab.com/rwr/spawner/wikis/home)  
[Todo list](https://gitlab.com/rwr/spawner/wikis/TODO)  
[Discord](https://discord.gg/ufHXN2x)  

LICENCE
=======

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
