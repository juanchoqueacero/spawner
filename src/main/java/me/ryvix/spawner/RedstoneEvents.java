/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class RedstoneEvents implements Listener {

	/**
	 * Listen to BlockRedstoneEvent to check spawner power.
	 *
	 * @param event BlockPhysicsEvent
	 */
	@EventHandler
	public void onBlockRedstoneEvent(BlockRedstoneEvent event) {
		RedstoneFunctions.redstoneCheck(event.getBlock());
	}

	/**
	 * Listen to BlockPistonExtendEvent to check spawner power.
	 *
	 * @param event BlockPistonExtendEvent
	 */
	@EventHandler
	public void onBlockPistonExtendEvent(final BlockPistonExtendEvent event) {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Block block : event.getBlocks()) {
					RedstoneFunctions.redstoneCheck(block.getRelative(event.getDirection()));
				}
			}
		}.runTaskLater(Main.instance, 4);
	}

	/**
	 * Listen to BlockPistonRetractEvent to check spawner power.
	 *
	 * @param event BlockPistonRetractEvent
	 */
	@EventHandler
	public void onBlockPistonRetractEvent(BlockPistonRetractEvent event) {
		if (!event.isSticky()) {
			return;
		}

		for (Block block : event.getBlocks()) {
			RedstoneFunctions.redstoneCheck(block);
		}
	}
}