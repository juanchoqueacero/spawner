/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Spawner extends ItemStack {

	private String entityName;
	private EntityType spawnerType;

	public Spawner(ItemStack item) {
		super(item);
	}

	Spawner(int amount) {
		super(Material.SPAWNER, amount);
		initSpawner();
	}

	Spawner(ItemStack item, int amount) {
		super(item);
		initSpawner();
		item.setAmount(amount);
	}

	private void initSpawner() {
		removeEnchantment(Enchantment.SILK_TOUCH);
	}

	/**
	 * Set formatted display name
	 *
	 * @param name String
	 */
	public void setFormattedName(String name) {
		// Set display name from NBT or language.
		if (name == null || name.isEmpty()) {
			name = getEntityName();
		}

		setEntityName(name);

		ItemMeta im = getItemMeta();
		im.setDisplayName(Main.instance.getLangHandler().colorText(Main.instance.getLangHandler().translateEntity(name, "value") + " " + Main.instance.getLangHandler().getText("Spawner")));
		setItemMeta(im);
	}

	/**
	 * Get display name
	 *
	 * @return String
	 */
	public String getName() {

		ItemMeta im = getItemMeta();
		String name = im.getDisplayName();
		if (name == null) {
			name = "";
		}

		return name;
	}

	/**
	 * Set display name
	 *
	 * @param name String
	 * @return Spawner
	 */
	public Spawner setName(String name) {

		ItemMeta im = getItemMeta();
		im.setDisplayName(name);
		setItemMeta(im);

		return this;
	}

	public String getEntityName() {
		if (entityName == null) {
			entityName = Main.instance.getNmsHandler().getEntityNameFromSpawnerNBT(this);
			if (entityName == null || entityName.isEmpty()) {
				entityName = "";
			} else {
				entityName = SpawnerFunctions.getSplitValue(entityName);
			}
		}
		return SpawnerFunctions.convertAlias(entityName);
	}

	public void setEntityName(String entityName) {
		this.entityName = ChatColor.stripColor(Main.instance.getLangHandler().colorText(entityName));
	}

	public String getFormattedEntityName() {
		return Main.instance.getLangHandler().translateEntity(getEntityName(), "value");
	}

	public EntityType getSpawnerType() {
		// if spawner type is null try setting it
		if (this.spawnerType == null) {
			String name = getEntityName();
			setSpawnerType(name);
		}

		return this.spawnerType;
	}

	public void setSpawnerType(String entityName) {
		this.spawnerType = SpawnerFunctions.fromCleanName(SpawnerFunctions.convertAlias(entityName));
	}

}
