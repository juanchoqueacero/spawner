package me.ryvix.spawner;

import com.vdurmont.semver4j.Semver;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.*;

public class ConfigUpdater {
	private String oldVersion;
	private String versionPath = "";
	private Semver sem;
	private boolean fromOldVersion = false;

	public ConfigUpdater() {
		this("");
	}

	public ConfigUpdater(String oldVersion) {
		this.oldVersion = oldVersion;
		init();
		updateConfig();
		updateLanguage();
		updateCommands();
		updateEntities();
		Main.instance.reloadFiles();
	}

	private void init() {
		String version = (String) Main.instance.getConfigHandler().getConfigValue("version", "string");
		if (version == null) {
			version = "0.0.1";
		}

		// Check if an old version arg was passed.
		List<String> validOldVersions = Arrays.asList("v1_10_R1", "v1_11_R1", "v1_12_R1");
		if (validOldVersions.contains(oldVersion)) {
			fromOldVersion = true;

			// Some old versions stored config files in subfolders.
			versionPath = File.separator + oldVersion + File.separator;
		}

		// Use Semver4j (https://github.com/vdurmont/semver4j) to compare config versions
		sem = new Semver(version);
	}

	/**
	 * Update config.yml
	 */
	private void updateConfig() {

		// Migrate from old version.
		if (fromOldVersion || sem.isLowerThan("3.0.0")) {

			// Get the config file as a CustomConfig.
			CustomConfig oldConfig = new CustomConfig(versionPath + "config.yml");

			// Convert valid_entities section to entities.yml
			if (oldConfig.contains("valid_entities")) {

				List<String> valid_entities = oldConfig.getStringList("valid_entities");


				// Get a list of entities from the entities.yml file
				Set<String> entityList = SpawnerFunctions.getEntities(false, false);

				// Loop through each entity in the entities.yml file
				for (String entity : entityList) {

					// Is this entity enabled?
					if (!valid_entities.contains(entity) && !valid_entities.contains(EntityMap.valueOf(entity).get_v1_11()) && !valid_entities.contains(EntityMap.valueOf(entity).get_v1_10())) {
						Main.instance.getConfigHandler().getEntities().set("entities." + entity + ".enabled", false);
					}

					// Set aliases.
					List<String> aliases = Main.instance.getConfigHandler().getEntities().getStringList("entities." + entity + ".aliases");
					if (oldConfig.contains("aliases." + EntityMap.valueOf(entity).get_v1_11())) {
						// v1_11
						aliases.addAll(oldConfig.getStringList("aliases." + EntityMap.valueOf(entity).get_v1_11()));

					} else if (oldConfig.contains("aliases." + EntityMap.valueOf(entity).get_v1_10())) {
						// v1_10
						aliases.addAll(oldConfig.getStringList("aliases." + EntityMap.valueOf(entity).get_v1_10()));
					}
					Main.instance.getConfigHandler().getEntities().set("entities." + entity + ".aliases", aliases);

					// Get old frequency
					String frequency = "global";
					if (oldConfig.contains("frequency." + EntityMap.valueOf(entity).get_v1_11())) {
						// v1_11
						frequency = oldConfig.getString("frequency." + EntityMap.valueOf(entity).get_v1_11());

					} else if (oldConfig.contains("frequency." + EntityMap.valueOf(entity).get_v1_10())) {
						// v1_10
						frequency = oldConfig.getString("frequency." + EntityMap.valueOf(entity).get_v1_10());
					}

					// Set frequency for this entity
					if (frequency.equals("global")) {
						Main.instance.getConfigHandler().getEntities().set("entities." + entity + ".frequency", frequency);
					} else {
						Main.instance.getConfigHandler().getEntities().set("entities." + entity + ".frequency", Integer.parseInt(frequency));
					}

					// Set drops.
					ConfigurationSection newDrops = Main.instance.getConfigHandler().getEntities().createSection("entities." + entity + ".drops");
					ConfigurationSection oldDrops = oldConfig.getConfigurationSection("drops");
					for (String key : oldDrops.getKeys(false)) {

						int amount;
						int data;
						String type;
						boolean silk;
						Material eggMaterial = Material.PIG_SPAWN_EGG;

						String oldKey = entity;
						if (key.equalsIgnoreCase(EntityMap.valueOf(entity).get_v1_11())) {
							oldKey = key;
						} else if (key.equalsIgnoreCase(EntityMap.valueOf(entity).get_v1_10())) {
							oldKey = key;
						}

						if (oldDrops.contains(oldKey + ".type")
								&& oldDrops.contains(oldKey + ".amount")
								&& oldDrops.contains(oldKey + ".data")
								&& oldDrops.contains(oldKey + ".silk")) {

							type = oldDrops.getString(oldKey + ".type");
							amount = oldDrops.getInt(oldKey + ".amount");
							data = oldDrops.getInt(oldKey + ".data");
							silk = oldDrops.getBoolean(oldKey + ".silk");

							// Translate eggs.
							if (type.equals("MONSTER_EGG")) {
								switch (data) {
									case 1:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 2:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 3:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 4:
										eggMaterial = Material.ELDER_GUARDIAN_SPAWN_EGG;
										break;
									case 5:
										eggMaterial = Material.WITHER_SKELETON_SPAWN_EGG;
										break;
									case 6:
										eggMaterial = Material.STRAY_SPAWN_EGG;
										break;
									case 7:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 8:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 9:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 10:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 11:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 12:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 13:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 14:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 15:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 16:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 17:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 18:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 19:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 20:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 21:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 22:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 23:
										eggMaterial = Material.HUSK_SPAWN_EGG;
										break;
									case 24:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 25:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 26:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 27:
										eggMaterial = Material.ZOMBIE_VILLAGER_SPAWN_EGG;
										break;
									case 28:
										eggMaterial = Material.SKELETON_HORSE_SPAWN_EGG;
										break;
									case 29:
										eggMaterial = Material.ZOMBIE_HORSE_SPAWN_EGG;
										break;
									case 30:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 31:
										eggMaterial = Material.DONKEY_SPAWN_EGG;
										break;
									case 32:
										eggMaterial = Material.MULE_SPAWN_EGG;
										break;
									case 33:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 34:
										eggMaterial = Material.EVOKER_SPAWN_EGG;
										break;
									case 35:
										eggMaterial = Material.VEX_SPAWN_EGG;
										break;
									case 36:
										eggMaterial = Material.VINDICATOR_SPAWN_EGG;
										break;
									case 37:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 40:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 41:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 42:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 43:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 44:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 45:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 46:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 47:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 50:
										eggMaterial = Material.CREEPER_SPAWN_EGG;
										break;
									case 51:
										eggMaterial = Material.SKELETON_SPAWN_EGG;
										break;
									case 52:
										eggMaterial = Material.SPIDER_SPAWN_EGG;
										break;
									case 53:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 54:
										eggMaterial = Material.ZOMBIE_SPAWN_EGG;
										break;
									case 55:
										eggMaterial = Material.SLIME_SPAWN_EGG;
										break;
									case 56:
										eggMaterial = Material.GHAST_SPAWN_EGG;
										break;
									case 57:
										eggMaterial = Material.ZOMBIFIED_PIGLIN_SPAWN_EGG;
										break;
									case 58:
										eggMaterial = Material.ENDERMAN_SPAWN_EGG;
										break;
									case 59:
										eggMaterial = Material.CAVE_SPIDER_SPAWN_EGG;
										break;
									case 60:
										eggMaterial = Material.SILVERFISH_SPAWN_EGG;
										break;
									case 61:
										eggMaterial = Material.BLAZE_SPAWN_EGG;
										break;
									case 62:
										eggMaterial = Material.MAGMA_CUBE_SPAWN_EGG;
										break;
									case 63:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 64:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 65:
										eggMaterial = Material.BAT_SPAWN_EGG;
										break;
									case 66:
										eggMaterial = Material.WITCH_SPAWN_EGG;
										break;
									case 67:
										eggMaterial = Material.ENDERMITE_SPAWN_EGG;
										break;
									case 68:
										eggMaterial = Material.GUARDIAN_SPAWN_EGG;
										break;
									case 69:
										eggMaterial = Material.SHULKER_SPAWN_EGG;
										break;
									case 90:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 91:
										eggMaterial = Material.SHEEP_SPAWN_EGG;
										break;
									case 92:
										eggMaterial = Material.COW_SPAWN_EGG;
										break;
									case 93:
										eggMaterial = Material.CHICKEN_SPAWN_EGG;
										break;
									case 94:
										eggMaterial = Material.SQUID_SPAWN_EGG;
										break;
									case 95:
										eggMaterial = Material.WOLF_SPAWN_EGG;
										break;
									case 96:
										eggMaterial = Material.MOOSHROOM_SPAWN_EGG;
										break;
									case 97:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 98:
										eggMaterial = Material.OCELOT_SPAWN_EGG;
										break;
									case 99:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 100:
										eggMaterial = Material.HORSE_SPAWN_EGG;
										break;
									case 101:
										eggMaterial = Material.RABBIT_SPAWN_EGG;
										break;
									case 102:
										eggMaterial = Material.POLAR_BEAR_SPAWN_EGG;
										break;
									case 103:
										eggMaterial = Material.LLAMA_SPAWN_EGG;
										break;
									case 104:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									case 105:
										eggMaterial = Material.PARROT_SPAWN_EGG;
										break;
									case 120:
										eggMaterial = Material.VILLAGER_SPAWN_EGG;
										break;
									case 200:
										eggMaterial = Material.PIG_SPAWN_EGG;
										break;
									default:
										break;
								}
							}

							LinkedHashMap<String, Object> values = new LinkedHashMap<>();
							values.put("amount", amount);
							values.put("silk", silk);
							newDrops.createSection(eggMaterial.name(), values);
						}
					}
				}
			}

			// If we were using the root config.yml then some old values have to be removed and the version has to be set.
			if (versionPath.isEmpty()) {
				// Remove valid_entities section from config.yml
				Main.instance.getConfig().set("valid_entities", null);

				// Set default global frequency
				Main.instance.getConfig().set("frequency", 100);

				// Set default global drops
				Main.instance.getConfig().createSection("drops");

				// Add global economy option
				Main.instance.getConfig().set("economy", false);

				// Add global tabcomplete option
				Main.instance.getConfig().set("tabcomplete", true);

				// Add global search_distance option
				Main.instance.getConfig().set("search_distance", 20);

				// Add global power_with_redstone option
				Main.instance.getConfig().set("power_with_redstone", false);
			}

			// Migrate other options from old config.
			if (oldConfig.contains("prevent_break_if_inventory_full")) {
				Main.instance.getConfig().set("prevent_break_if_inventory_full", oldConfig.get("prevent_break_if_inventory_full"));
			}
			if (oldConfig.contains("break_into_inventory")) {
				Main.instance.getConfig().set("break_into_inventory", oldConfig.get("break_into_inventory"));
			}
			if (oldConfig.contains("luck")) {
				Main.instance.getConfig().set("luck", oldConfig.get("luck"));
			}
			if (oldConfig.contains("remove_radius")) {
				Main.instance.getConfig().set("remove_radius", oldConfig.get("remove_radius"));
			}
			if (oldConfig.contains("drop_from_explosions")) {
				Main.instance.getConfig().set("drop_from_explosions", oldConfig.get("drop_from_explosions"));
			}
			if (oldConfig.contains("protect_from_explosions")) {
				Main.instance.getConfig().set("protect_from_explosions", oldConfig.get("protect_from_explosions"));
			}
		}

		// Config 3.0.1
		if (sem.isLowerThanOrEqualTo("3.0.1")) {
			// Added cooldown option.
			if (!Main.instance.getConfig().isSet("cooldown")) {
				Main.instance.getConfig().set("cooldown", false);
			}
		}

		// Config 3.0.5
		if (sem.isLowerThanOrEqualTo("3.0.5")) {
			// Added invert_redstone option.
			if (!Main.instance.getConfig().isSet("invert_redstone")) {
				Main.instance.getConfig().set("invert_redstone", false);
			}
		}

		// Config 3.0.10
		if (sem.isLowerThanOrEqualTo("3.0.10")) {
			// Added redstone_particles option.
			if (!Main.instance.getConfig().isSet("redstone_particles")) {
				Main.instance.getConfig().set("redstone_particles", true);
			}
		}

		// Config 3.0.11
		if (sem.isLowerThanOrEqualTo("3.0.11")) {
			// Added advanced_redstone option.
			if (!Main.instance.getConfig().isSet("advanced_redstone")) {
				Main.instance.getConfig().set("advanced_redstone", true);
			}

			// Added advanced_redstone_max_dpeth option.
			if (!Main.instance.getConfig().isSet("advanced_redstone_max_depth")) {
				Main.instance.getConfig().set("advanced_redstone_max_depth", 15);
			}
		}

		// Config 3.0.12
		if (sem.isLowerThanOrEqualTo("3.0.12")) {
			// Save default drops brackets properly
			if (!Main.instance.getConfig().isSet("drops") || (Main.instance.getConfig().isConfigurationSection("drops") && Main.instance.getConfig().getConfigurationSection("drops") != null && Main.instance.getConfig().getConfigurationSection("drops").getKeys(false).isEmpty())) {
				Main.instance.getConfig().set("drops", new ArrayList<>());
			}
		}

		// Update config version from plugin version.
		Semver pluginVersion = new Semver(Main.instance.getPluginVersion());
		String versionString = pluginVersion.withClearedSuffixAndBuild().getValue();
		if (sem.isLowerThan(versionString)) {
			Main.instance.getConfig().set("version", versionString);
		}

		// Save config.yml
		Main.instance.saveConfig();
	}

	/**
	 * Update commands.yml
	 */
	private void updateCommands() {

		// Config 3.0.2
		if (sem.isLowerThanOrEqualTo("3.0.2")) {
			// Added missing config command.
			if (!Main.instance.getConfigHandler().getCommands().isSet("commands.config")) {
				LinkedHashMap<String, Object> values = new LinkedHashMap<>();
				values.put("cost", 0);
				values.put("cooldown", 1);
				Main.instance.getConfigHandler().getCommands().createSection("commands.config", values);
			}
		}

		Main.instance.getConfigHandler().getCommands().save();
	}

	/**
	 * Update entities.yml
	 */
	private void updateEntities() {

		// Save entities.yml
		Main.instance.getConfigHandler().getEntities().save();

		// Config 3.0.5
		if (sem.isLowerThanOrEqualTo("3.0.5")) {

			// Added cat.
			addEntity("cat");

			// Added panda.
			addEntity("panda");

			// Added pillager.
			addEntity("pillager");

			// Added ravager.
			addEntity("ravager");

			// Added trader_llama.
			addEntity("trader_llama");

			// Added wandering_trader.
			addEntity("wandering_trader");

			// Added fox.
			addEntity("fox");

			// Added fishing_bobber.
			addEntity("fishing_bobber", false);

			// Added lightning_bolt.
			addEntity("lightning_bolt", false);
		}

		// Config 3.0.6
		if (sem.isLowerThanOrEqualTo("3.0.6")) {

			// Added bee.
			addEntity("bee");
		}

		// Config 3.0.7
		if (sem.isLowerThanOrEqualTo("3.0.7")) {

			// Converted zombie_pigman to zombified_piglin.
			if (Main.instance.getConfigHandler().getEntities().isSet("entities.zombie_pigman")) {
				ConfigurationSection zombie_pigman = Main.instance.getConfigHandler().getEntities().getConfigurationSection("entities.zombie_pigman");
				if (zombie_pigman != null) {
					Main.instance.getConfigHandler().getEntities().createSection("entities.zombified_piglin", zombie_pigman.getValues(false));
					Main.instance.getConfigHandler().getEntities().set("entities.zombie_pigman", null);
				}
			} else {
				// Add zombified_piglin if zombie_pigman wasn't found in the config.
				addEntity("zombified_piglin");
			}

			// Added hoglin.
			addEntity("hoglin");

			// Added piglin.
			addEntity("piglin");

			// Added strider.
			addEntity("strider");

			// Added zoglin.
			addEntity("zoglin");
		}

		Main.instance.getConfigHandler().getEntities().save();
	}

	private void addEntity(String entity) {
		addEntity(entity, true);
	}

	private void addEntity(String entity, boolean enabled) {
		if (!Main.instance.getConfigHandler().getEntities().isSet("entities." + entity)) {
			LinkedHashMap<String, Object> values = new LinkedHashMap<>();
			values.put("enabled", enabled);
			values.put("aliases", new ArrayList<>());
			values.put("drops", "global");
			values.put("frequency", "global");
			values.put("luck", "global");
			values.put("remove_radius", "global");
			values.put("protect_from_explosions", "global");
			values.put("drop_from_explosions", "global");
			values.put("break_into_inventory", "global");
			values.put("prevent_break_if_inventory_full", "global");
			values.put("power_with_redstone", "global");
			values.put("set_cost", "global");
			values.put("give_cost", "global");
			Main.instance.getConfigHandler().getEntities().createSection("entities." + entity, values);
		}
	}

	/**
	 * Update language.yml
	 */
	private void updateLanguage() {

		if (fromOldVersion || sem.isLowerThan("3.0.0")) {
			// Get the language file as a CustomConfig.
			CustomConfig oldLang = new CustomConfig(versionPath + File.separator + "language.yml");

			// Copy the language section from the old config because it's the same but with new additions that will be added later.
			if (oldLang.contains("Language")) {
				Main.instance.getConfigHandler().getLanguage().set("Language", oldLang.get("Language"));
			}

			// Get Entities section.
			ConfigurationSection entitiesSection = Main.instance.getConfigHandler().getLanguage().getConfigurationSection("Entities");

			// Get a list of entities from the entities.yml file
			Set<String> entityList = SpawnerFunctions.getEntities(false, false);

			// Loop through each entity in the entities.yml file and map to the new entity keys.
			for (String entity : entityList) {
				if (oldLang.contains("Entities." + EntityMap.valueOf(entity).get_v1_11())) {
					// v1_11
					entitiesSection.set(entity, oldLang.getString("Entities." + EntityMap.valueOf(entity).get_v1_11()));

				} else if (oldLang.contains("Entities." + EntityMap.valueOf(entity).get_v1_10())) {
					// v1_10
					entitiesSection.set(entity, oldLang.getString("Entities." + EntityMap.valueOf(entity).get_v1_10()));
				}
			}

			// Add any missing entries.
			if (!Main.instance.getConfigHandler().getLanguage().contains("Language")) {
				Main.instance.getConfigHandler().getLanguage().createSection("Language");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.LookAtASpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.LookAtASpawner", "&4Look at a spawner to see what kind it is!");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.NoPermission")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.NoPermission", "&4You don't have permission to do that!");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.SpawnerChangedTo")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.SpawnerChangedTo", "&aSpawner type changed to {0}");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.InvalidSpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.InvalidSpawner", "&4Invalid spawner type!");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.GivenSpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.GivenSpawner", "&aYou were given a {0} &aspawner.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.SpawnerDropped")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.SpawnerDropped", "&aA {0} spawner was dropped at your feet because your inventory is full.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.YouGaveSpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.YouGaveSpawner", "&aYou gave a {0} &aspawner to {1}.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.NotDeliveredOffline")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.NotDeliveredOffline", "&4The spawner was not delivered because &e{0} &4is offline.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.YouPickedUp")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.YouPickedUp", "&aYou picked up a {0} &aspawner.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.HoldingSpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.HoldingSpawner", "&aYou are holding a {0} &aspawner.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.SpawnerType")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.SpawnerType", "&aSpawner type: {0}");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.PlacedSpawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.PlacedSpawner", "&aPlaced {0} &aspawner");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.NotPossible")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.NotPossible", "&4It was not possible to change that spawner.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.InvalidRadius")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.InvalidRadius", "&4You entered an invalid radius.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.InvalidEntity")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.InvalidEntity", "&4You entered an invalid entity.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.ErrorRemovingEntities")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.ErrorRemovingEntities", "&4There was an error removing entities. Some may not have been removed.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.EntitiesRemoved")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.EntitiesRemoved", "&a{0} {1} &aremoved.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.Spawner")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.Spawner", "&aspawner");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.NoCreative")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.NoCreative", "&4Sorry but you can't pick that up in creative mode.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.InventoryFull")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.InventoryFull", "&4Your inventory is full.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.CurrencyCharged")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.CurrencyCharged", "&aYou were charged &e{0}. You have &e{1} left in your account.");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.NotEnoughCurrency")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.NotEnoughCurrency", "&4Not enough currency in your account!");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.OnCooldown")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.OnCooldown", "&4Cooldown remaining: &e{0}");
			}

			// Entities
			if (!Main.instance.getConfigHandler().getLanguage().contains("Entities")) {
				Main.instance.getConfigHandler().getLanguage().createSection("Entities");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.bat")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.bat", "&ebat");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.item")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.item", "&eitem");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.experience_orb")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.experience_orb", "&eexperience_orb");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.area_effect_cloud")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.area_effect_cloud", "&earea_effect_cloud");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.elder_guardian")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.elder_guardian", "&eelder_guardian");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.wither_skeleton")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.wither_skeleton", "&ewither_skeleton");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.stray")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.stray", "&estray");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.egg")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.egg", "&eegg");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.leash_knot")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.leash_knot", "&eleash_knot");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.painting")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.painting", "&epainting");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.arrow")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.arrow", "&earrow");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.snowball")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.snowball", "&esnowball");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.fireball")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.fireball", "&efireball");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.small_fireball")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.small_fireball", "&esmall_fireball");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.ender_pearl")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.ender_pearl", "&eender_pearl");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.eye_of_ender")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.eye_of_ender", "&eeye_of_ender");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.potion")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.potion", "&epotion");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.experience_bottle")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.experience_bottle", "&eexperience_bottle");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.item_frame")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.item_frame", "&eitem_frame");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.wither_skull")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.wither_skull", "&ewither_skull");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.tnt")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.tnt", "&etnt");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.falling_block")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.falling_block", "&efalling_block");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.firework_rocket")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.firework_rocket", "&efirework_rocket");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.husk")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.husk", "&ehusk");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.spectral_arrow")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.spectral_arrow", "&espectral_arrow");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.shulker_bullet")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.shulker_bullet", "&eshulker_bullet");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.dragon_fireball")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.dragon_fireball", "&edragon_fireball");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.zombie_villager")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.zombie_villager", "&ezombie_villager");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.skeleton_horse")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.skeleton_horse", "&eskeleton_horse");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.zombie_horse")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.zombie_horse", "&ezombie_horse");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.armor_stand")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.armor_stand", "&earmor_stand");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.donkey")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.donkey", "&edonkey");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.mule")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.mule", "&emule");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.evoker_fangs")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.evoker_fangs", "&eevoker_fangs");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.evoker")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.evoker", "&eevoker");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.vex")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.vex", "&evex");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.vindicator")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.vindicator", "&evindicator");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.illusioner")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.illusioner", "&eillusioner");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.command_block_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.command_block_minecart", "&ecommand_block_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.boat")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.boat", "&eboat");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.minecart", "&eminecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.chest_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.chest_minecart", "&echest_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.furnace_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.furnace_minecart", "&efurnace_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.tnt_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.tnt_minecart", "&etnt_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.hopper_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.hopper_minecart", "&ehopper_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.spawner_minecart")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.spawner_minecart", "&espawner_minecart");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.creeper")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.creeper", "&ecreeper");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.skeleton")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.skeleton", "&eskeleton");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.spider")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.spider", "&espider");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.giant")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.giant", "&egiant");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.zombie")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.zombie", "&ezombie");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.slime")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.slime", "&eslime");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.ghast")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.ghast", "&eghast");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.zombie_pigman")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.zombie_pigman", "&ezombie_pigman");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.enderman")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.enderman", "&eenderman");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.cave_spider")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.cave_spider", "&ecave_spider");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.silverfish")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.silverfish", "&esilverfish");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.blaze")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.blaze", "&eblaze");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.magma_cube")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.magma_cube", "&emagma_cube");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.ender_dragon")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.ender_dragon", "&eender_dragon");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.wither")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.wither", "&ewither");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.bat")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.bat", "&ebat");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.witch")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.witch", "&ewitch");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.endermite")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.endermite", "&eendermite");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.guardian")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.guardian", "&eguardian");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.shulker")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.shulker", "&eshulker");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.pig")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.pig", "&epig");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.sheep")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.sheep", "&esheep");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.cow")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.cow", "&ecow");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.chicken")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.chicken", "&echicken");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.squid")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.squid", "&esquid");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.wolf")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.wolf", "&ewolf");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.mooshroom")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.mooshroom", "&emooshroom");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.snow_golem")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.snow_golem", "&esnow_golem");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.ocelot")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.ocelot", "&eocelot");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.iron_golem")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.iron_golem", "&eiron_golem");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.horse")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.horse", "&ehorse");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.rabbit")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.rabbit", "&erabbit");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.polar_bear")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.polar_bear", "&epolar_bear");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.llama")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.llama", "&ellama");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.llama_spit")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.llama_spit", "&ellama_spit");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.parrot")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.parrot", "&eparrot");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.villager")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.villager", "&evillager");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.end_crystal")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.end_crystal", "&eend_crystal");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.turtle")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.turtle", "&eturtle");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.phantom")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.phantom", "&ephantom");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.trident")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.trident", "&etrident");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.cod")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.cod", "&ecod");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.salmon")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.salmon", "&esalmon");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.pufferfish")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.pufferfish", "&epufferfish");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.tropical_fish")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.tropical_fish", "&etropical_fish");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.drowned")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.drowned", "&edrowned");
			}
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.dolphin")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.dolphin", "&edolphin");
			}

			// Remove any extra entries.
			if (Main.instance.getConfigHandler().getLanguage().isSet("Language.ConsoleUsageGive")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.ConsoleUsageGive", null);
			}
		}

		// Config 3.0.1
		if (sem.isLowerThanOrEqualTo("3.0.1")) {
			// Added Success message.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.Success")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.Success", "&aSuccess!");
			}

			// Added Failed message.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Language.Failed")) {
				Main.instance.getConfigHandler().getLanguage().set("Language.Failed", "&4Failed!");
			}
		}

		// Config 3.0.5
		if (sem.isLowerThanOrEqualTo("3.0.5")) {
			// Added cat.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.cat")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.cat", "&ecat");
			}

			// Added panda.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.panda")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.panda", "&epanda");
			}

			// Added pillager.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.pillager")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.pillager", "&epillager");
			}

			// Added ravager.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.ravager")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.ravager", "&eravager");
			}

			// Added trader_llama.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.trader_llama")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.trader_llama", "&etrader_llama");
			}

			// Added wandering_trader.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.wandering_trader")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.wandering_trader", "&ewandering_trader");
			}

			// Added fishing_bobber.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.fishing_bobber")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.fishing_bobber", "&efishing_bobber");
			}

			// Added lightning_bolt.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.lightning_bolt")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.lightning_bolt", "&elightning_bolt");
			}
		}

		// Config 3.0.6
		if (sem.isLowerThanOrEqualTo("3.0.6")) {
			// Added bee.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.bee")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.bee", "&ebee");
			}
		}

		// Config 3.0.7
		if (sem.isLowerThanOrEqualTo("3.0.7")) {

			// Converted zombie_pigman to zombified_piglin.
			if (Main.instance.getConfigHandler().getLanguage().isSet("Entities.zombie_pigman")) {
				String zombie_pigman = Main.instance.getConfigHandler().getLanguage().getString("Entities.zombie_pigman");
				if (zombie_pigman != null) {
					Main.instance.getConfigHandler().getLanguage().set("Entities.zombified_piglin", zombie_pigman);
					Main.instance.getConfigHandler().getLanguage().set("Entities.zombie_pigman", null);
				}
			} else {
				// Add zombified_piglin if zombie_pigman wasn't found in the config.
				Main.instance.getConfigHandler().getLanguage().set("Entities.zombified_piglin", "&ezombified_piglin");
			}

			// Added hoglin.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.hoglin")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.hoglin", "&ehoglin");
			}
			// Added piglin.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.piglin")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.piglin", "&epiglin");
			}
			// Added strider.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.strider")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.strider", "&estrider");
			}
			// Added zoglin.
			if (!Main.instance.getConfigHandler().getLanguage().isSet("Entities.zoglin")) {
				Main.instance.getConfigHandler().getLanguage().set("Entities.zoglin", "&ezoglin");
			}
		}

		// Save language.yml
		Main.instance.getConfigHandler().getLanguage().save();
	}
}
