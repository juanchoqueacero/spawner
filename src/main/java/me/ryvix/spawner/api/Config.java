/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.api;

import me.ryvix.spawner.CustomConfig;

/**
 * Config API
 */
public interface Config {
	/**
	 * Get a value from the config.
	 *
	 * @param key  String
	 * @param type String
	 * @return Object
	 */
	Object getConfigValue(String key, String type);

	/**
	 * Get a value from the config. Set spawner to an empty string or null to get a global value.
	 *
	 * @param key     String
	 * @param spawner String
	 * @param type    String
	 * @return Object
	 */
	Object getConfigValue(String key, String spawner, String type);

	void loadFiles();

	void reloadFiles();

	void loadConfig();

	CustomConfig getLanguage();

	void setLanguage(CustomConfig language);

	CustomConfig getCommands();

	void setCommands(CustomConfig commands);

	CustomConfig getEntities();

	void setEntities(CustomConfig entities);

	CustomConfig getCooldowns();

	void setCooldowns(CustomConfig cooldowns);

}
