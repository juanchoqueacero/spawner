/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import me.ryvix.spawner.api.Language;
import net.minecraft.server.v1_16_R3.IChatBaseComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_16_R3.util.CraftChatMessage;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LangHandler implements Language {

	/**
	 * @param s String
	 * @return net.md_5.bungee.api.ChatColor
	 * @see <a href="https://gitlab.com/rwr/spawner/-/issues/59#note_370351916">https://gitlab.com/rwr/spawner/-/issues/59#note_370351916</a>
	 */
	public static net.md_5.bungee.api.ChatColor getColor(String s) {
		try {
			Class c = net.md_5.bungee.api.ChatColor.class;
			Method m = c.getMethod("of", String.class);
			Object o = m.invoke(null, s);
			return (net.md_5.bungee.api.ChatColor) o;

		} catch (Exception e) {
			return net.md_5.bungee.api.ChatColor.WHITE;
		}
	}

	/**
	 * @param s String
	 * @return String
	 * @see <a href="https://gitlab.com/rwr/spawner/-/issues/59#note_370199765">https://gitlab.com/rwr/spawner/-/issues/59#note_370199765</a>
	 */
	public static String convertColors(String s) {
		Pattern pattern = Pattern.compile(Pattern.quote("{#") + "(.*?)" + Pattern.quote("}"));
		Matcher match = pattern.matcher(s);
		String ns = s;
		while (match.find()) {
			String colorcode = match.group(1);
			ns = ns.replaceAll("\\{#" + colorcode + "\\}", getColor("#" + colorcode).toString());
		}

		return ns;
	}

	/**
	 * Colorizes input string
	 *
	 * @param input Input string to colorize.
	 * @return String
	 */
	public String colorText(String input) {
		input = ChatColor.translateAlternateColorCodes("&".charAt(0), input);

		input = convertColors(input);

		IChatBaseComponent cmp = CraftChatMessage.fromString(input, true)[0];
		return CraftChatMessage.fromComponent(cmp);
	}

	/**
	 * Get text
	 *
	 * @param key  String
	 * @param vars Optional String array
	 * @return String
	 */
	public String getText(String key, String... vars) {
		String text;
		try {
			text = Main.instance.getConfigHandler().getLanguage().getString("Language." + key);
		} catch (Exception e) {
			Main.instance.getLogger().log(Level.WARNING, e.getLocalizedMessage());
			Main.instance.getLogger().log(Level.WARNING, e.toString());
			Main.instance.getLogger().log(Level.WARNING, "Missing language at: Language.{0}", key);
			Main.instance.getLogger().log(Level.WARNING, "Add it or regenerate your language.yml by deleting the file and then run /spawner reload");

			// return nothing
			return "";
		}

		// check for empty text
		if (text == null || text.isEmpty()) {
			return "";
		}

		for (int x = 0; x < vars.length; x++) {
			text = text.replace("{" + x + "}", vars[x]);
		}

		return text;
	}

	/**
	 * Get entity
	 *
	 * @param entity String
	 * @return String
	 */
	public String getEntity(String entity) {

		String text;
		try {
			text = Main.instance.getConfigHandler().getLanguage().getString("Entities." + entity);
		} catch (Exception e) {
			Main.instance.getLogger().log(Level.WARNING, e.getLocalizedMessage());
			Main.instance.getLogger().log(Level.WARNING, e.toString());
			Main.instance.getLogger().log(Level.WARNING, "Missing language at: Entities.{0}", entity);
			Main.instance.getLogger().log(Level.WARNING, "Add it or regenerate your language.yml by deleting it and then run /spawner reload");

			// return default spawner name
			return "Monster";
		}

		// check for empty text
		if (text.isEmpty()) {
			return "";
		}

		return text;
	}

	/**
	 * Send a message to a Player if it isn't empty.
	 * Supports color codes, i.e. &amp;4Red text
	 *
	 * @param playerUuid UUID
	 * @param text       String
	 */
	public void sendMessage(UUID playerUuid, String text) {
		if (!text.isEmpty()) {
			Player player = Main.instance.getServer().getPlayer(playerUuid);
			if (player != null) {
				player.sendMessage(colorText(text));
			}
		}
	}

	/**
	 * Send a message to a CommandSender if it isn't empty.
	 * Supports color codes, i.e. &amp;4Red text
	 *
	 * @param sender CommandSender
	 * @param text   String
	 */
	public void sendMessage(CommandSender sender, String text) {
		if (!text.isEmpty()) {
			sender.sendMessage(colorText(text));
		}
	}

	/**
	 * Translate entity names to their keys or values.
	 *
	 * @param inputName String
	 * @param type      returns either the key or value
	 * @return String
	 */
	public String translateEntity(String inputName, String type) {
		String returnName = "default";

		if (type == null) {
			return returnName;
		}

		inputName = ChatColor.stripColor(colorText(inputName));

		ConfigurationSection csEntities;
		Set<Map.Entry<String, Object>> entityValues;
		try {
			csEntities = Main.instance.getConfigHandler().getLanguage().getConfigurationSection("Entities");
			entityValues = csEntities.getValues(false).entrySet();
		} catch (Exception e) {
			Main.instance.getLogger().severe("Your Spawner language.yml is missing entities in the Entities section. This is probably because it's outdated. You can update it manually or to install a new one you can rename or delete the old one. Once finished run the command /spawner reload");
			return null;
		}

		for (Map.Entry<String, Object> entry : entityValues) {
			String testKey = ChatColor.stripColor(colorText("" + entry.getKey()));
			String testValue = ChatColor.stripColor(colorText("" + entry.getValue()));
			if (testValue.equalsIgnoreCase(inputName) || testKey.equalsIgnoreCase(inputName)) {
				switch (type) {
					case "value":
						returnName = "" + entry.getValue();
						break;
					case "key":
						returnName = testKey;
						break;
				}
				break;
			}
		}

		return returnName;
	}
}
