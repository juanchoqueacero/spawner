package me.ryvix.spawner;

/**
 * Map of entities between versions.
 * <p>
 * In MC 1.11 entity names changed. Some of them changed again in 1.13. For backwards compatibility they must be mapped.
 * <p>
 * 1.11: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/diff/src/main/java/org/bukkit/entity/EntityType.java?until=dd1c703c9ff5fcaf625b479c5f800489663fc746
 * 1.13: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/diff/src/main/java/org/bukkit/entity/EntityType.java?until=f8b2086d60942eb2cd7ac25a2a1408cb790c222c
 * 1.16: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/diff/src/main/java/org/bukkit/entity/EntityType.java?until=eb5480423dfe8987b55cd408de434a54a9d327f4
 * <p>
 * Format:
 * v1_16(v1_13, v1_11, v1_10)
 */
public enum EntityMap {
	item("item", "item", "Item"),
	experience_orb("experience_orb", "xp_orb", "XPOrb"),
	area_effect_cloud("area_effect_cloud", "area_effect_cloud", null),
	elder_guardian("elder_guardian", "elder_guardian", null),
	wither_skeleton("wither_skeleton", "wither_skeleton", null),
	stray("stray", "stray", null),
	egg("egg", "egg", "egg"),
	leash_knot("leash_knot", "leash_knot", "LeashKnot"),
	painting("painting", "painting", "Painting"),
	arrow("arrow", "arrow", "Arrow"),
	snowball("snowball", "snowball", "Snowball"),
	fireball("fireball", "fireball", "Fireball"),
	small_fireball("small_fireball", null, null),
	ender_pearl("ender_pearl", "ender_pearl", "ThrownEnderpearl"),
	eye_of_ender("eye_of_ender", "eye_of_ender_signal", "EyeOfEnderSignal"),
	potion("potion", "potion", "potion"),
	experience_bottle("experience_bottle", "xp_bottle", "ThrownExpBottle"),
	item_frame("item_frame", "item_frame", "ItemFrame"),
	wither_skull("wither_skull", "wither_skull", "WitherSkull"),
	tnt("tnt", "tnt", "PrimedTnt"),
	falling_block("falling_block", "falling_block", "FallingSand"),
	firework_rocket("firework_rocket", "fireworks_rocket", "FireworksRocketEntity"),
	husk("husk", "husk", null),
	spectral_arrow("spectral_arrow", "spectral_arrow", "SpectralArrow"),
	shulker_bullet("shulker_bullet", "shulker_bullet", "ShulkerBullet"),
	dragon_fireball("dragon_fireball", "dragon_fireball", "DragonFireball"),
	zombie_villager("zombie_villager", "zombie_villager", null),
	skeleton_horse("skeleton_horse", "skeleton_horse", null),
	zombie_horse("zombie_horse", "zombie_horse", null),
	armor_stand("armor_stand", "armor_stand", "ArmorStand"),
	donkey("donkey", "donkey", null),
	mule("mule", "mule", null),
	evoker_fangs("evoker_fangs", "evocation_fangs", null),
	evoker("evoker", "evocation_illager", null),
	vex("vex", "vex", null),
	vindicator("vindicator", "vindication_illager", null),
	illusioner("illusioner", "illusion_illager", null),
	command_block_minecart("command_block_minecart", "commandblock_minecart", "MinecartCommandBlock"),
	boat("boat", "boat", "Boat"),
	minecart("minecart", "minecart", "MinecartRideable"),
	chest_minecart("chest_minecart", "chest_minecart", "MinecartChest"),
	furnace_minecart("furnace_minecart", "furnace_minecart", "MinecartFurnace"),
	tnt_minecart("tnt_minecart", "tnt_minecart", "MinecartTNT"),
	hopper_minecart("hopper_minecart", "hopper_minecart", "MinecartHopper"),
	spawner_minecart("spawner_minecart", "spawner_minecart", "MinecartMobSpawner"),
	creeper("creeper", "creeper", "Creeper"),
	skeleton("skeleton", "skeleton", "Skeleton"),
	spider("spider", "spider", "Spider"),
	giant("giant", "giant", "Giant"),
	zombie("zombie", "zombie", "Zombie"),
	slime("slime", "slime", "Slime"),
	ghast("ghast", "ghast", "Ghast"),
	zombified_piglin("zombie_pigman", "zombie_pigman", "PigZombie"),
	enderman("enderman", "enderman", "Enderman"),
	cave_spider("cave_spider", "cave_spider", "CaveSpider"),
	silverfish("silverfish", "silverfish", "Silverfish"),
	blaze("blaze", "blaze", "Blaze"),
	magma_cube("magma_cube", "magma_cube", "LavaSlime"),
	ender_dragon("ender_dragon", "ender_dragon", "EnderDragon"),
	wither("wither", "wither", "WitherBoss"),
	bat("bat", "bat", "Bat"),
	witch("witch", "witch", "Witch"),
	endermite("endermite", "endermite", "Endermite"),
	guardian("guardian", "guardian", "Guardian"),
	shulker("shulker", "shulker", "Shulker"),
	pig("pig", "pig", "Pig"),
	sheep("sheep", "sheep", "Sheep"),
	cow("cow", "cow", "Cow"),
	chicken("chicken", "chicken", "Chicken"),
	squid("squid", "squid", "Squid"),
	wolf("wolf", "wolf", "Wolf"),
	mooshroom("mooshroom", "mooshroom", "MushroomCow"),
	snow_golem("snow_golem", "snowman", "SnowMan"),
	ocelot("ocelot", "ocelot", "Ozelot"),
	iron_golem("iron_golem", "villager_golem", "VillagerGolem"),
	horse("horse", "horse", "EntityHorse"),
	rabbit("rabbit", "rabbit", "Rabbit"),
	polar_bear("polar_bear", "polar_bear", "PolarBear"),
	llama("llama", "llama", null),
	llama_spit("llama_spit", "llama_spit", null),
	parrot("parrot", "parrot", null),
	villager("villager", "villager", "Villager"),
	end_crystal("end_crystal", "ender_crystal", "EnderCrystal"),
	turtle(null, null, null),
	phantom(null, null, null),
	trident(null, null, null),
	cod(null, null, null),
	salmon(null, null, null),
	pufferfish(null, null, null),
	tropical_fish(null, null, null),
	drowned(null, null, null),
	dolphin(null, null, null),
	cat(null, null, null),
	panda(null, null, null),
	pillager(null, null, null),
	ravager(null, null, null),
	trader_llama(null, null, null),
	wandering_trader(null, null, null),
	fox(null, null, null),
	fishing_bobber(null, null, null),
	lightning_bolt(null, null, null),
	bee(null, null, null),
	hoglin(null, null, null),
	piglin(null, null, null),
	strider(null, null, null),
	zoglin(null, null, null),
	player(null, null, null);

	private final String v1_13;
	private final String v1_11;
	private final String v1_10;

	EntityMap(String v1_13, String v1_11, String v1_10) {
		this.v1_13 = v1_13;
		this.v1_11 = v1_11;
		this.v1_10 = v1_10;
	}

	private String v1_13() {
		return v1_13;
	}

	private String v1_11() {
		return v1_11;
	}

	private String v1_10() {
		return v1_10;
	}

	public String get_v1_13() {
		return v1_13;
	}

	public String get_v1_11() {
		return v1_11;
	}

	public String get_v1_10() {
		return v1_10;
	}
}
