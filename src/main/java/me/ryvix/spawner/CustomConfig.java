/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * CustomConfig class
 * <p>
 * Credit: https://www.spigotmc.org/threads/class-simple-custom-configs.51124/
 */
public class CustomConfig extends YamlConfiguration {

	private File file;
	private String defaults;

	public CustomConfig(String fileName) {
		this(fileName, null);
	}

	public CustomConfig(String fileName, String defaultsName) {
		this.defaults = defaultsName;
		this.file = new File(Main.instance.getDataFolder(), fileName);
		reload();
	}

	public void reload() {
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();

			} catch (IOException exception) {
				exception.printStackTrace();
				Main.instance.getLogger().severe("Error creating file " + file.getName());
			}
		}

		InputStreamReader reader = null;
		try {
			load(file);

			if (defaults != null) {
				reader = new InputStreamReader(Main.instance.getResource(defaults), StandardCharsets.UTF_8);
				FileConfiguration defaultsConfig = YamlConfiguration.loadConfiguration(reader);

				setDefaults(defaultsConfig);
				options().copyDefaults(true);

				reader.close();
				save();
			}

		} catch (IOException | InvalidConfigurationException exception) {
			exception.printStackTrace();
			Main.instance.getLogger().severe("Error loading file " + file.getName());

		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ex) {
					Main.instance.getLogger().severe("Error closing file " + file.getName());
				}
			}
		}
	}

	public void save() {
		try {
			options().indent(2);
			save(file);

		} catch (IOException exception) {
			exception.printStackTrace();
			Main.instance.getLogger().severe("Error saving file " + file.getName());
		}
	}
}
