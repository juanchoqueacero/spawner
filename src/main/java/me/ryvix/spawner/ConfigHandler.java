/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import me.ryvix.spawner.api.Config;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.logging.Level;

/**
 * ConfigHandler class
 *
 * @see <a href="https://www.spigotmc.org/wiki/config-files/">https://www.spigotmc.org/wiki/config-files/</a>
 * @see <a href="https://www.spigotmc.org/threads/making-a-subfolders-to-a-plugin.93062/">https://www.spigotmc.org/threads/making-a-subfolders-to-a-plugin.93062/</a>
 */
public class ConfigHandler implements Config {

	private File configFile;
	private CustomConfig language;
	private CustomConfig commands;
	private CustomConfig entities;
	private CustomConfig cooldowns;

	/**
	 * Get values from the config
	 *
	 * @param key  String
	 * @param type String
	 * @return Object | null
	 */
	@Override
	public Object getConfigValue(String key, String type) {
		return getConfigValue(key, null, type);
	}

	/**
	 * Get values from the config
	 *
	 * @param key     String
	 * @param spawner String
	 * @param type    String
	 * @return Object | null
	 */
	@Override
	public Object getConfigValue(String key, String spawner, String type) {
		String value;

		try {

			// Checking a global config value if spawner is empty.
			if (spawner == null || spawner.isEmpty()) {
				value = "global";

			} else {
				String entities_path = "entities." + spawner.toLowerCase() + "." + key;
				if (!type.equals("ConfigurationSection")) {
					value = Main.instance.getConfigHandler().getEntities().getString(entities_path);

				} else {
					if (Main.instance.getConfigHandler().getEntities().getString(entities_path) == null || (Main.instance.getConfigHandler().getEntities().isString(entities_path) && Main.instance.getConfigHandler().getEntities().getString(entities_path).equals("global"))) {
						return Main.instance.getConfig().getConfigurationSection(key);
					} else {
						ConfigurationSection pathSection = Main.instance.getConfigHandler().getEntities().getConfigurationSection(entities_path);
						if (pathSection != null && pathSection.getKeys(false).size() == 0) {
							return Main.instance.getConfig().getConfigurationSection(key);
						}
						return Main.instance.getConfigHandler().getEntities().getConfigurationSection(entities_path);
					}
				}
			}

			if (value == null || value.equals("global")) {
				switch (type) {
					case "string":
						return Main.instance.getSpawnerConfig().getString(key);
					case "boolean":
						return Main.instance.getSpawnerConfig().getBoolean(key);
					case "int":
						return Main.instance.getSpawnerConfig().getInt(key);
					case "float":
						return Float.parseFloat(Main.instance.getSpawnerConfig().getString(key));
					case "double":
						return Double.parseDouble(Main.instance.getSpawnerConfig().getString(key));
				}

			} else {
				switch (type) {
					case "string":
						return key;
					case "boolean":
						return Boolean.parseBoolean(value);
					case "int":
						return Integer.parseInt(value);
					case "float":
						return Float.parseFloat(value);
					case "double":
						return Double.parseDouble(value);
				}
			}

		} catch (Exception e) {
			Main.instance.getLogger().warning("Possible missing config entry for " + key + ". Please update your config! Error follows.");
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Load Spawner's files from disk.
	 */
	@Override
	public void loadFiles() {
		configFile = new File(Main.instance.getDataFolder() + File.separator, "config.yml");

		// load config file
		Main.instance.setSpawnerConfig(null);
		loadConfig();

		// load language file
		language = new CustomConfig("language.yml", "language.yml");

		// load commands file
		commands = new CustomConfig("commands.yml", "commands.yml");

		// load entities file
		entities = new CustomConfig("entities.yml", "entities.yml");

		// load cooldowns file
		cooldowns = new CustomConfig("cooldowns.yml", "cooldowns.yml");

		// create help and list files
		try {
			File file = new File(Main.instance.getDataFolder(), "help.txt");
			if (!file.exists()) {
				Main.instance.saveResource("help.txt", false);
			}
		} catch (Exception ex) {
			Main.instance.getLogger().log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Reload Spawner's files from disk after they've already been loaded before.
	 */
	@Override
	public void reloadFiles() {
		// reload config file
		Main.instance.setSpawnerConfig(null);
		Main.instance.setSpawnerConfig(YamlConfiguration.loadConfiguration(configFile));

		// reload language file
		language.reload();

		// reload commands file
		commands.reload();

		// reload entities file
		entities.reload();

		// reload cooldowns file
		cooldowns.reload();
	}

	/**
	 * Load config values
	 */
	@Override
	public void loadConfig() {
		// create config file
		Main.instance.saveDefaultConfig();

		// set config file
		Main.instance.setSpawnerConfig(YamlConfiguration.loadConfiguration(configFile));
	}

	public CustomConfig getLanguage() {
		return language;
	}

	public void setLanguage(CustomConfig language) {
		this.language = language;
	}

	public CustomConfig getCommands() {
		return commands;
	}

	public void setCommands(CustomConfig commands) {
		this.commands = commands;
	}

	public CustomConfig getEntities() {
		return entities;
	}

	public void setEntities(CustomConfig entities) {
		this.entities = entities;
	}

	public CustomConfig getCooldowns() {
		return cooldowns;
	}

	public void setCooldowns(CustomConfig cooldowns) {
		this.cooldowns = entities;
	}
}
