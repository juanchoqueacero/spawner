/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import me.ryvix.spawner.api.NMS;
import net.minecraft.server.v1_16_R3.*;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_16_R3.util.CraftChatMessage;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

// TODO: show SpawnPotentials data in item lore
public class NMSHandler implements NMS {

	/**
	 * Set minimal default NBT data.
	 *
	 * @param spawner Spawner
	 * @param name    String
	 * @return Spawner
	 */
	public Spawner setDefaultNBT(Spawner spawner, String name) {

		// Set spawner name.
		spawner.setFormattedName(name);

		// NMS ItemStack
		net.minecraft.server.v1_16_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(spawner);

		// Get NBT tag from item and update version.
		NBTTagCompound tagNBT = nmsStack.getOrCreateTag();

		// NBT BlockEntityTag
		NBTTagCompound blockEntityTagNBT = new NBTTagCompound();

		// NBT SpawnData
		NBTTagCompound spawnDataNBT = new NBTTagCompound();

		// NBT Spawndata.id
		spawnDataNBT.setString("id", "minecraft:" + name);

		// Save SpawnData to the NBT tag.
		blockEntityTagNBT.set("SpawnData", spawnDataNBT);

		// NBT SpawnPotentials
		NBTTagCompound spawnPotentialsNBT = new NBTTagCompound();

		// Entity
		spawnPotentialsNBT.set("Entity", new NBTTagCompound());
		NBTTagCompound spawnPotentialsEntityNBT = spawnPotentialsNBT.getCompound("Entity");
		spawnPotentialsEntityNBT.setString("id", "minecraft:" + name);

		// Weight
		spawnPotentialsNBT.setInt("Weight", 1);

		// Store the data in a list.
		NBTTagList spawnPotentialsNBTTagList = new NBTTagList();
		spawnPotentialsNBTTagList.add(spawnPotentialsNBT);

		// Save SpawnPotentials list to BlockEntityTag.
		blockEntityTagNBT.set("SpawnPotentials", spawnPotentialsNBTTagList);

		// Set more default values.
		blockEntityTagNBT.setShort("SpawnRange", (short) 4);
		blockEntityTagNBT.setShort("Delay", (short) -1);
		blockEntityTagNBT.setShort("SpawnCount", (short) 4);
		blockEntityTagNBT.setShort("MaxSpawnDelay", (short) 800);
		blockEntityTagNBT.setShort("MinSpawnDelay", (short) 200);
		blockEntityTagNBT.setShort("MaxNearbyEntities", (short) 6);
		blockEntityTagNBT.setShort("RequiredPlayerRange", (short) 16);

		// Save BlockEntityTag NBT.
		tagNBT.set("BlockEntityTag", blockEntityTagNBT);

		// Save NBT tag to item.
		nmsStack.setTag(tagNBT);

		// Convert to ItemStack.
		ItemStack stack = new ItemStack(Material.SPAWNER, nmsStack.getCount());
		stack.setItemMeta(CraftItemStack.getItemMeta(nmsStack));

		return new Spawner(stack);
	}

	/**
	 * Set Block NBT from Spawner item.
	 *
	 * @param spawner Spawner
	 * @param block   Block
	 * @return boolean
	 */
	@Override
	public boolean setSpawnerBlockNBT(Spawner spawner, Block block) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return false;
		}

		// Make sure the spawner has NBT set. The spawner should always have a name at this point.
		spawner = setSpawnerNBT(spawner, spawner.getEntityName());

		// NMS ItemStack
		net.minecraft.server.v1_16_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(spawner);
		NBTTagCompound tagNBT = nmsStack.getTag();

		if (tagNBT != null) {

			// Get tile entity from the world.
			CraftWorld craftWorld = (CraftWorld) block.getWorld();
			TileEntityMobSpawner tileEntity = (TileEntityMobSpawner) craftWorld.getHandle().getTileEntity(new BlockPosition(block.getX(), block.getY(), block.getZ()));

			if (tileEntity != null) {
				MobSpawnerAbstract mobSpawner = tileEntity.getSpawner();

				// BlockEntityTag
				NBTTagCompound blockEntityTag = tagNBT.getCompound("BlockEntityTag");

				// Assemble spawner data.
				NBTTagCompound spawnerTag = new NBTTagCompound();
				spawnerTag.set("SpawnPotentials", blockEntityTag.getList("SpawnPotentials", spawnerTag.getTypeId()));
				spawnerTag.set("SpawnData", blockEntityTag.getCompound("SpawnData"));
				spawnerTag.setShort("Delay", blockEntityTag.getShort("Delay"));
				spawnerTag.setShort("MaxNearbyEntities", blockEntityTag.getShort("MaxNearbyEntities"));
				spawnerTag.setShort("MinSpawnDelay", blockEntityTag.getShort("MinSpawnDelay"));
				spawnerTag.setShort("MaxSpawnDelay", blockEntityTag.getShort("MaxSpawnDelay"));
				spawnerTag.setShort("RequiredPlayerRange", blockEntityTag.getShort("RequiredPlayerRange"));
				spawnerTag.setShort("SpawnCount", blockEntityTag.getShort("SpawnCount"));
				spawnerTag.setShort("SpawnRange", blockEntityTag.getShort("SpawnRange"));

				// Set spawner data.
				mobSpawner.a(spawnerTag);

				// Update block state in the world.
				block.getState().update();
			}

		} else {
			return false;
		}

		return true;
	}

	/**
	 * Set spawner type using NBT
	 *
	 * @param spawner Spawner
	 * @return CraftItemStack
	 * @see <a href="https://minecraft.gamepedia.com/Spawner#Block_entity">https://minecraft.gamepedia.com/Spawner#Block_entity</a>
	 */
	@Override
	public Spawner setSpawnerNBT(Spawner spawner, String name) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return null;
		}

		// Gets entity name from spawner using various methods and then gets a clean version using the language key.
		String cleanEntity = Main.instance.getLangHandler().translateEntity(spawner.getEntityName(), "key");

		// The name variable should be a valid entity at this point so use it as the default if necessary.
		if (cleanEntity.equals("default")) {
			cleanEntity = name.toLowerCase();
		}

		// Set default NBT.
		spawner = setDefaultNBT(spawner, cleanEntity);

		// NMS ItemStack
		net.minecraft.server.v1_16_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(spawner);

		// Set NBT data from config for each spawner type if necessary.
		// entities.yml entities.<entity>.tag
		ConfigurationSection tagSection = (ConfigurationSection) Main.instance.getConfigHandler().getConfigValue("tag", cleanEntity, "ConfigurationSection");
		if (tagSection != null) {

			// Get NBT tag from item and update version.
			NBTTagCompound tagNBT = nmsStack.getOrCreateTag();

			// NBT BlockEntityTag
			NBTTagCompound blockEntityTagNBT = new NBTTagCompound();

			// entities.yml entities.<entity>.tag.blockentitytag
			if (tagSection.contains("blockentitytag")) {

				// entities.yml entities.<entity>.tag.spawndata.id
				String tag_spawndata_id_value = "minecraft:" + cleanEntity;
				if (tagSection.contains("spawndata.id") && !tagSection.getString("spawndata.id").equals("")) {

					String tag_spawndata_id = tagSection.getString("spawndata.id");

					// Validate Spawndata.id
					if (!tag_spawndata_id.isEmpty()) {
						tag_spawndata_id_value = tag_spawndata_id;
					}
				}

				// NBT SpawnData (overwritten after next spawn attempt if SpawnPotentials is set)
				NBTTagCompound spawnDataNBT = new NBTTagCompound();

				// NBT Spawndata.id
				spawnDataNBT.setString("id", tag_spawndata_id_value);

				// Save SpawnData to NBT tag.
				blockEntityTagNBT.set("SpawnData", spawnDataNBT);

				// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials
				if (tagSection.contains("blockentitytag.spawnpotentials")) {

					ConfigurationSection spawnPotentials = tagSection.getConfigurationSection("blockentitytag.spawnpotentials");
					Set<String> spawnPotentialsSet = spawnPotentials.getKeys(false);
					NBTTagList spawnPotentialsNBTTagList = new NBTTagList();

					// Loop through each spawnpotential # set for this entity in entities.yml
					for (String spawnPotentialKey : spawnPotentialsSet) {
						NBTTagCompound spawnPotentialsNBT = new NBTTagCompound();
						String spawnPotentialPath = "blockentitytag.spawnpotentials." + spawnPotentialKey + ".";

						// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.entity.id
						// Note: Overwrites SpawnData and SpawnData.id NBT
						if (tagSection.contains(spawnPotentialPath + "entity.id") && !tagSection.getString(spawnPotentialPath + "entity.id").equals("")) {
							String tag_blockentitytag_spawnpotentials_entity_id = tagSection.getString(spawnPotentialPath + "entity.id");

							// Validate entity type.
							if (SpawnerFunctions.getEntities(false, false).contains(tag_blockentitytag_spawnpotentials_entity_id)) {

								// NBT BlockEntityTag.SpawnPotentials.#.Entity
								spawnPotentialsNBT.set("Entity", new NBTTagCompound());
								NBTTagCompound spawnPotentialsEntityNBT = spawnPotentialsNBT.getCompound("Entity");
								spawnPotentialsEntityNBT.setString("id", tag_blockentitytag_spawnpotentials_entity_id);

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.invulnerable
								if (tagSection.contains(spawnPotentialPath + "invulnerable")) {
									boolean tag_blockentitytag_spawnpotentials_invulnerable = tagSection.getBoolean(spawnPotentialPath + "invulnerable");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Invulnerable
									spawnPotentialsEntityNBT.setBoolean("Invulnerable", tag_blockentitytag_spawnpotentials_invulnerable);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.customname
								if (tagSection.contains(spawnPotentialPath + "customname")) {
									String tag_blockentitytag_spawnpotentials_customname = Main.instance.getLangHandler().colorText(tagSection.getString(spawnPotentialPath + "customname"));

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.CustomName
									spawnPotentialsEntityNBT.set("CustomName", NBTTagString.a(CraftChatMessage.toJSON(CraftChatMessage.fromStringOrNull(tag_blockentitytag_spawnpotentials_customname))));
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.customnamevisible
								if (tagSection.contains(spawnPotentialPath + "customnamevisible")) {
									boolean tag_blockentitytag_spawnpotentials_customnamevisible = tagSection.getBoolean(spawnPotentialPath + "customnamevisible");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.CustomNameVisible
									spawnPotentialsEntityNBT.setBoolean("CustomNameVisible", tag_blockentitytag_spawnpotentials_customnamevisible);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.silent
								if (tagSection.contains(spawnPotentialPath + "silent")) {
									boolean tag_blockentitytag_spawnpotentials_silent = tagSection.getBoolean(spawnPotentialPath + "silent");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Silent
									spawnPotentialsEntityNBT.setBoolean("Silent", tag_blockentitytag_spawnpotentials_silent);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.glowing
								if (tagSection.contains(spawnPotentialPath + "glowing")) {
									boolean tag_blockentitytag_spawnpotentials_glowing = tagSection.getBoolean(spawnPotentialPath + "glowing");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Glowing
									spawnPotentialsEntityNBT.setBoolean("Glowing", tag_blockentitytag_spawnpotentials_glowing);
								}

								// TODO: Add more entity tags

							} else {
								Main.instance.getLogger().warning("Invalid value in entities.yml at entities." + cleanEntity + ".tag." + spawnPotentialPath + "entity.id. If this is a valid entity id make sure it exists in entities.yml.");
							}
						}

						// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.weight
						int tag_blockentitytag_spawnpotentials_weight;
						if (tagSection.contains(spawnPotentialPath + "weight")) {
							tag_blockentitytag_spawnpotentials_weight = tagSection.getInt(spawnPotentialPath + "weight");

							// Must be greater or equal to 1.
							if (tag_blockentitytag_spawnpotentials_weight >= 1) {

								// NBT BlockEntityTag.SpawnPotentials.#.Weight
								spawnPotentialsNBT.setInt("Weight", tag_blockentitytag_spawnpotentials_weight);
							}
						}
						spawnPotentialsNBTTagList.add(spawnPotentialsNBT);
					}

					// NBT BlockEntityTag.SpawnPotentials.#
					blockEntityTagNBT.set("SpawnPotentials", spawnPotentialsNBTTagList);
				}
			}

			// entities.yml entities.<entity>.tag.spawnrange
			int tag_spawnrange_value = 4;
			if (tagSection.contains("spawnrange")) {
				int tag_spawnrange = tagSection.getInt("spawnrange");

				// Validate SpawnRange.
				if (tag_spawnrange > 0) {
					tag_spawnrange_value = tag_spawnrange;
				}
			}

			// NBT SpawnRange
			blockEntityTagNBT.setShort("SpawnRange", (short) tag_spawnrange_value);

			// entities.yml entities.<entity>.tag.delay
			int tag_delay_value = -1;
			if (tagSection.contains("delay")) {
				int tag_delay = tagSection.getInt("delay");

				// Validate Delay.
				if (tag_delay == -1 || tag_delay > 0) {
					tag_delay_value = tag_delay;
				}
			}

			// NBT Delay
			blockEntityTagNBT.setShort("Delay", (short) tag_delay_value);

			// entities.yml entities.<entity>.tag.minspawndelay
			// Note: NBT is set further down to check that it isn't greater than MaxSpawnDelay which depends on it.
			int tag_minspawndelay = 0;
			int tag_minspawndelay_value = 200;
			if (tagSection.contains("minspawndelay")) {
				tag_minspawndelay = tagSection.getInt("minspawndelay");
			}

			// entities.yml entities.<entity>.tag.spawncount
			// Note: MinSpawnDelay must be set.
			int tag_spawncount_value = 4;
			if (tagSection.contains("spawncount")) {
				int tag_spawncount = tagSection.getInt("spawncount");

				// Validate SpawnCount.
				if (tag_spawncount > 0 && tag_minspawndelay > 0) {
					tag_spawncount_value = tag_spawncount;
				}
			}

			// NBT SpawnCount
			blockEntityTagNBT.setShort("SpawnCount", (short) tag_spawncount_value);

			// entities.yml entities.<entity>.tag.maxspawndelay
			// Note: MinSpawnDelay must be set.
			int tag_maxspawndelay = 1;
			int tag_maxspawndelay_value = 800;
			if (tagSection.contains("maxspawndelay")) {
				tag_maxspawndelay = tagSection.getInt("maxspawndelay");

				// Validate MaxSpawnDelay.
				// Setting to 0 crashes Minecraft.
				if (tag_maxspawndelay > 0 && tag_minspawndelay > 0) {
					tag_maxspawndelay_value = tag_maxspawndelay;
				}
			}

			// NBT MaxSpawnDelay
			blockEntityTagNBT.setShort("MaxSpawnDelay", (short) tag_maxspawndelay_value);

			// Validate MinSpawnDelay.
			if (tagSection.contains("minspawndelay")) {
				if (tag_minspawndelay > 0) {

					// Can be equal to MaxSpawnDelay but not greater than.
					if (tag_minspawndelay > tag_maxspawndelay) {
						tag_minspawndelay_value = tag_maxspawndelay;
					} else {
						tag_minspawndelay_value = tag_minspawndelay;
					}
				}
			}

			// NBT MinSpawnDelay
			blockEntityTagNBT.setShort("MinSpawnDelay", (short) tag_minspawndelay_value);

			// entities.yml entities.<entity>.tag.maxnearbyentities
			int tag_maxnearbyentities = 0;
			int tag_maxnearbyentities_value = 6;
			if (tagSection.contains("maxnearbyentities")) {
				tag_maxnearbyentities = tagSection.getInt("maxnearbyentities");

				// Validate MaxNearbyEntities.
				if (tag_maxnearbyentities > 0) {
					tag_maxnearbyentities_value = tag_maxnearbyentities;
				}
			}

			// NBT MaxNearbyEntities
			blockEntityTagNBT.setShort("MaxNearbyEntities", (short) tag_maxnearbyentities_value);

			// entities.yml entities.<entity>.tag.requiredplayerrange
			// Note: MaxNearbyEntities must be set.
			int tag_requiredplayerrange_value = 16;
			if (tagSection.contains("requiredplayerrange")) {
				int tag_requiredplayerrange = tagSection.getInt("requiredplayerrange");

				// Validate RequiredPlayerRange.
				if (tag_maxnearbyentities > 0) {
					tag_requiredplayerrange_value = tag_requiredplayerrange;
				}
			}

			// NBT RequiredPlayerRange
			blockEntityTagNBT.setShort("RequiredPlayerRange", (short) tag_requiredplayerrange_value);

			// Save BlockEntityTag to NBT tag.
			tagNBT.set("BlockEntityTag", blockEntityTagNBT);

			// entities.yml entities.<entity>.tag.display.name
			if (tagSection.contains("display.name") && !tagSection.getString("display.name").equals("")) {
				String tag_display_name = Main.instance.getLangHandler().colorText(tagSection.getString("display.name"));

				// NBT display
				NBTTagCompound displayNBT = new NBTTagCompound();

				// NBT display.Name
				displayNBT.set("Name", NBTTagString.a(CraftChatMessage.toJSON(CraftChatMessage.fromStringOrNull(tag_display_name))));

				// Save display to NBT tag.
				tagNBT.set("display", displayNBT);

			} else {
				spawner.setFormattedName(name);
			}

			// Save NBT tag to item.
			nmsStack.setTag(tagNBT);
		}

		ItemStack stack = new ItemStack(Material.SPAWNER, nmsStack.getCount());
		stack.setItemMeta(CraftItemStack.getItemMeta(nmsStack));

		return new Spawner(stack);

	}

	/**
	 * Get spawner type using NBT
	 *
	 * @return Entity Name string
	 */
	@Override
	public String getEntityNameFromSpawnerNBT(Spawner spawner) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return null;
		}

		net.minecraft.server.v1_16_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(spawner);

		if (nmsStack.hasTag()) {
			NBTTagCompound tag = nmsStack.getTag();

			if (tag != null) {

				NBTTagCompound tagToUse = new NBTTagCompound();
				if (tag.hasKey("SpawnData") || tag.hasKey("SpawnPotentials")) {
					tagToUse = tag;
				} else if (tag.hasKey("BlockEntityTag")) {
					tagToUse = tag.getCompound("BlockEntityTag");
				}

				if (tagToUse.hasKey("SpawnData")) {
					NBTTagCompound spawnData = tagToUse.getCompound("SpawnData");
					return getEntityIdFromTagCompound(spawnData);

				} else if (tagToUse.hasKey("SpawnPotentials")) {
					NBTTagList spawnPotentials = tagToUse.getList("SpawnPotentials", 10);
					if (!spawnPotentials.isEmpty()) {
						NBTTagCompound entity = spawnPotentials.getCompound(0).getCompound("Entity");
						return getEntityIdFromTagCompound(entity);
					}
				}
			}
		}

		return null;
	}

	/**
	 * Get entity tag from tag compound.
	 *
	 * @param tag NBTTagCompound
	 * @return String
	 */
	public String getEntityIdFromTagCompound(NBTTagCompound tag) {
		if (tag.hasKey("id")) {
			String entityTagId = tag.getString("id");
			return SpawnerFunctions.getSplitValue(entityTagId);
		}
		return "";
	}
}