/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands;

import me.ryvix.spawner.commands.spawner.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;

public class CommandSpawner implements TabExecutor {

	/**
	 * "/spawner" tab completion controller.
	 *
	 * @param sender Command sender
	 * @param cmd    Command
	 * @param label  Command alias
	 * @param args   Other parameters
	 * @return boolean
	 */
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		String cName = cmd.getName();
		List<String> results = new ArrayList<>();

		if (cName.equalsIgnoreCase("spawner")) {
			results.addAll(new Get(sender, args, true).tabComplete());
			results.addAll(new Give(sender, args, true).tabComplete());
			results.addAll(new Help(sender, args, true).tabComplete());
			results.addAll(new me.ryvix.spawner.commands.spawner.List(sender, args, true).tabComplete());
			results.addAll(new Reload(sender, args, true).tabComplete());
			results.addAll(new Remove(sender, args, true).tabComplete());
			results.addAll(new Set(sender, args, true).tabComplete());
			results.addAll(new Config(sender, args, true).tabComplete());
			return results;
		}

		return null;
	}

	/**
	 * "/spawner" command controller.
	 *
	 * @param sender Command sender
	 * @param cmd    Command
	 * @param label  Command alias
	 * @param args   Other parameters
	 * @return boolean
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		String cName = cmd.getName();

		if (cName.equalsIgnoreCase("spawner")) {
			new Get(sender, args, false);
			new Give(sender, args, false);
			new Help(sender, args, false);
			new me.ryvix.spawner.commands.spawner.List(sender, args, false);
			new Reload(sender, args, false);
			new Remove(sender, args, false);
			new Set(sender, args, false);
			new Config(sender, args, false);
		}

		return true;
	}
}