/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands;

import me.ryvix.spawner.Main;
import me.ryvix.spawner.SpawnerFunctions;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Subcommand {

	// Init. Fields.
	private boolean allowConsole;
	private Set<Integer> numArgs = new HashSet<>();
	private Set<String> permissions = new HashSet<>();
	private CommandSender sender = null;
	private String[] args;
	private int cooldown = 0;
	private long cooldownRemaining = 0;
	private double cost = 0;
	private long lastRun = 0;
	private EconomyResponse economyResponse;
	private Set<String> possibilities = new HashSet<>();
	private String spawner = null;
	private boolean getSpawnerType = false;
	private String uuid = "00000000-0000-0000-0000-000000000000";
	private boolean fromConsole;
	private boolean fromPlayer;

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public Subcommand(CommandSender sender, String[] args, boolean autocomplete) {

		// Init. variables.
		this.sender = sender;
		this.args = args;

		// Is this command sent from console?
		if (sender instanceof ConsoleCommandSender) {
			fromConsole = true;
		}

		// Is this command sent from a player?
		if (sender instanceof Player) {
			fromPlayer = true;
		}

		setDefaults();

		// If this is an autocomplete do nothing.
		if (autocomplete) {
			return;
		}

		// Only run for the right command.
		if (!isRightCommand()) {
			return;
		}

		// Make sure the valid number of args were sent.
		if (!ValidArgs()) {
			return;
		}

		// Catch allowConsole.
		if (!allowConsole && fromConsole) {
			Main.instance.getLangHandler().sendMessage(sender, getUsage("console"));
			return;
		} else if (allowConsole && fromConsole) {
			// Console should run the command.
			Command();
			return;
		}

		// Save player uuid.
		if (fromPlayer) {
			uuid = ((Player) sender).getUniqueId().toString();
		}

		// Cooldown
		if (Cooldown()) {

			// Permission
			if (Permission()) {

				// Has enough currency
				if (hasEnoughCurrency()) {

					// Spawner type
					if (SpawnerType()) {

						// Command
						if (Command()) {

							// Economy
							if (Cost()) {

								// If economyResponse is null then we didn't try to charge it so just return.
								if (economyResponse == null) {
									return;
								}

								// Send message about an economy response.
								Main.instance.getLangHandler().sendMessage(sender, Main.instance.getLangHandler().getText("CurrencyCharged", Main.instance.getEcon().format(economyResponse.amount), Main.instance.getEcon().format(economyResponse.balance)));

							} else {
								// Send message about an economy error.
								Main.instance.getLangHandler().sendMessage(sender, economyResponse.errorMessage);
							}
						}

					} else {
						// Invalid spawner type
						Main.instance.getLangHandler().sendMessage(sender, Main.instance.getLangHandler().getText("InvalidSpawner"));
					}

				} else {
					// Not enough currency.
					Main.instance.getLangHandler().sendMessage(sender, Main.instance.getLangHandler().getText("NotEnoughCurrency"));
				}

			} else {
				// No permissions
				Main.instance.getLangHandler().sendMessage(sender, Main.instance.getLangHandler().getText("NoPermission"));
			}

		} else {
			// On cooldown
			Main.instance.getLangHandler().sendMessage(sender, Main.instance.getLangHandler().getText("OnCooldown", Long.toString(getCooldownRemaining())));
		}
	}

	public void setDefaults() {
	}

	public boolean Command() {
		return true;
	}

	public boolean hasEnoughCurrency() {
		if (Main.instance.enableEconomy()) {
			Economy econ = Main.instance.getEcon();
			if (econ != null) {
				return econ.has((OfflinePlayer) sender, cost);
			} else {
				// If economy is enabled but null then all actions should fail.
				return false;
			}
		}
		return true;
	}

	/**
	 * Validates number of args sent
	 *
	 * @return boolean
	 */
	private boolean ValidArgs() {
		for (int numarg : numArgs) {
			if (this.args.length == numarg) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Cooldown logic
	 *
	 * @return boolean false if still on cooldown, true if ok to run command
	 */
	private boolean Cooldown() {

		// Check if there is a cooldown set for this command.
		if (cooldown <= 0) {
			return true;
		}

		// Check if cooldown support is enabled.
		if (!(boolean) Main.instance.getConfigHandler().getConfigValue("cooldown", "boolean")) {
			return true;
		}

		// Get lastRun. No need to save as a variable since it's saved to the lastRun field.
		getLastRun();

		// Bypass permission check.
		if (sender.hasPermission("spawner.cooldown.bypass")) {
			return true;
		}

		// Set timestamp to the current system time in seconds.
		long timestamp = System.currentTimeMillis() / 1000L;

		// Set the amount of time passed since the command was last run.
		long passed = timestamp - lastRun;

		// Set cooldown time remaining.
		setCooldownRemaining(cooldown - passed);

		// Check if there is any time remaining.
		if (getCooldownRemaining() > 0L) {
			return false;
		}

		// Set lastRun to current timestamp.
		setLastRun(timestamp);

		return true;
	}

	/**
	 * Permission logic
	 *
	 * @return boolean false if no permission, true if ok to run command (command should further check permissions)
	 */
	public boolean Permission() {
		for (String permission : permissions) {
			if (this.sender.hasPermission(permission)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Cost logic
	 *
	 * @return boolean false if player didn't have enough currency, true if player had enough currency
	 */
	private boolean Cost() {
		// Check if economy support is enabled.
		if (!Main.instance.enableEconomy()) {
			return true;
		}

		// Cost is nothing.
		if (cost == 0) {
			return true;
		}

		// If economy is enabled but null all actions should fail.
		Economy econ = Main.instance.getEcon();
		if (econ == null) {
			return false;
		}

		// Bypass permission check.
		if (sender.hasPermission("spawner.cost.bypass")) {
			return true;
		}

		// Withdrawl from player
		if (cost > 0) {
			economyResponse = econ.withdrawPlayer((OfflinePlayer) sender, cost);
		}

		// Deposit to player
		if (cost < 0) {
			economyResponse = econ.depositPlayer((OfflinePlayer) sender, cost);
		}

		return economyResponse.transactionSuccess();

	}

	/**
	 * Function to set the spawner type for this command
	 *
	 * @return boolean
	 */
	private boolean SpawnerType() {
		if (getSpawnerType) {
			Player player = (Player) sender;

			int searchDistance = (int) Main.instance.getConfigHandler().getConfigValue("search_distance", null, "int");
			searchDistance = SpawnerFunctions.normalizeViewDistance(searchDistance);

			Block target = SpawnerFunctions.findSpawnerBlock(player.getUniqueId(), searchDistance);

			if (target != null && target.getType() == Material.SPAWNER) {
				EntityType spawnerType = SpawnerFunctions.getSpawner(target);
				if (spawnerType == null) {
					return false;
				}
				spawner = spawnerType.name();
			}
		}

		return true;
	}

	/**
	 * Tab auto-completion
	 *
	 * @return Set Completions
	 */
	public Set<String> tabComplete() {
		// Show for console?
		if (!isAllowConsole() && fromConsole()) {
			return new HashSet<>();
		}

		Set<String> completions = new TreeSet<>();

		// Check if tabcomplete is enabled in the config first.
		boolean tabComplete = (boolean) Main.instance.getConfigHandler().getConfigValue("tabcomplete", null, "boolean");
		if (tabComplete) {

			// Check if sender has permission.
			if (Permission()) {

				// Handle different results for different number of args passed.
				if (getArgs().length == 1) {
					// /spawner <cmd>

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(0), getPossibilities(), completions);
				}
			}
		}
		return completions;
	}

	/**
	 * Get command usage info.
	 *
	 * @param topic String
	 * @return usage String
	 */
	public String getUsage(String topic) {
		if (topic.equals("console")) {
			return "spawner give <entity> <player>";
		}
		return "/spawner help";
	}

	// Get the command string from the class name.
	public String getCommand() {
		return this.getClass().getSimpleName().toLowerCase();
	}

	// Make sure the first arg is the right command - for autocompletion.
	public boolean isRightCommand() {
		// If no args use Get
		if (getArgs().length == 0) {
			return true;
		}
		return getArgs(0).equalsIgnoreCase(getCommand());
	}

	// Getters and setters
	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public String getArgs(int index) {
		return args[index];
	}

	public long getCooldownRemaining() {
		return cooldownRemaining;
	}

	public void setCooldownRemaining(long cooldownRemaining) {
		this.cooldownRemaining = cooldownRemaining;
	}

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public Set<String> getPermission() {
		return permissions;
	}

	public void setPermission(Set<String> permissions) {
		this.permissions = permissions;
	}

	public void addPermission(String permission) {
		this.permissions.add(permission);
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public long getLastRun() {
		if (Main.instance.getConfigHandler().getCooldowns().contains(uuid)) {
			ConfigurationSection playerCooldowns = Main.instance.getConfigHandler().getCooldowns().getConfigurationSection(uuid);
			if (playerCooldowns != null) {
				if (playerCooldowns.contains(getCommand())) {
					lastRun = playerCooldowns.getLong(getCommand());
				}
			}
		}

		return lastRun;
	}

	public void setLastRun(long lastRun) {
		Main.instance.getConfigHandler().getCooldowns().set(uuid + "." + getCommand(), lastRun);
		Main.instance.getConfigHandler().getCooldowns().save();
		this.lastRun = lastRun;
	}

	public boolean isAllowConsole() {
		return allowConsole;
	}

	public void setAllowConsole(boolean allowConsole) {
		this.allowConsole = allowConsole;
	}

	public Set<Integer> getNumArgs() {
		return numArgs;
	}

	public void setNumArgs(Set<Integer> numArgs) {
		this.numArgs = numArgs;
	}

	public void addNumArgs(int num) {
		numArgs.add(num);
	}

	public EconomyResponse getEconomyResponse() {
		return economyResponse;
	}

	public void setEconomyResponse(EconomyResponse economyResponse) {
		this.economyResponse = economyResponse;
	}

	public CommandSender getSender() {
		return sender;
	}

	public void setSender(CommandSender sender) {
		this.sender = sender;
	}

	public Set<String> getPossibilities() {
		return possibilities;
	}

	public void setPossibilities(Set<String> possibilities) {
		this.possibilities = possibilities;
	}

	public void addPossibility(String possibility) {
		this.possibilities.add(possibility);
	}

	public String getSpawner() {
		return spawner;
	}

	public void setSpawner(String spawner) {
		this.spawner = spawner;
	}

	public boolean isGetSpawnerType() {
		return getSpawnerType;
	}

	public void setGetSpawnerType(boolean getSpawnerType) {
		this.getSpawnerType = getSpawnerType;
	}

	public boolean fromConsole() {
		return fromConsole;
	}

	public void setFromConsole(boolean fromConsole) {
		this.fromConsole = fromConsole;
	}

	public boolean fromPlayer() {
		return fromPlayer;
	}

	public void setFromPlayer(boolean fromPlayer) {
		this.fromPlayer = fromPlayer;
	}
}
