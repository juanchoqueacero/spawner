/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands.spawner;

import me.ryvix.spawner.Main;
import me.ryvix.spawner.SpawnerFunctions;
import me.ryvix.spawner.commands.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Remove extends Subcommand {

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public Remove(CommandSender sender, String[] args, boolean autocomplete) {
		super(sender, args, autocomplete);
	}

	/**
	 * Set default variables for this command.
	 */
	@Override
	public void setDefaults() {
		// Allow allowConsole to run this command?
		setAllowConsole(false);

		// Valid number of args array.
		addNumArgs(2);
		addNumArgs(3);

		// Valid permissions array.
		addPermission("spawner.remove");

		// Add commands to tab completion list.
		addPossibility("remove");

		// Set cooldown from config.
		setCooldown(Main.instance.getConfigHandler().getCommands().getInt("commands.remove.cooldown"));

		// Set cost from config.
		setCost(Main.instance.getConfigHandler().getCommands().getDouble("commands.remove.cost"));
	}

	/**
	 * Command logic
	 *
	 * @return boolean false if command was not run, true if it ran successfully
	 */
	@Override
	public boolean Command() {
		if (getArgs().length == 2) {

			// /spawner remove <entity>
			if (fromPlayer()) {
				if (SpawnerFunctions.isValidEntityName(getArgs(1))) {
					String entity = getArgs(1).toLowerCase();
					int radius = (int) Main.instance.getConfigHandler().getConfigValue("remove_radius", entity, "int");
					if (radius > 0) {
						Player player = (Player) getSender();
						SpawnerFunctions.removeEntities(player.getUniqueId(), entity, radius);
					} else {
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					}
				} else {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}
			}

		} else if (getArgs().length == 3) {

			// /spawner remove <entity> <radius>
			if (fromPlayer()) {
				if (SpawnerFunctions.isValidEntityName(getArgs(1))) {
					int radius;
					try {
						radius = Integer.parseInt(getArgs(2));
					} catch (NumberFormatException e) {
						Main.instance.getLangHandler().sendMessage(getSender(), ChatColor.RED + Main.instance.getLangHandler().getText("InvalidRadius"));
						return false;
					}

					Player player = (Player) getSender();
					SpawnerFunctions.removeEntities(player.getUniqueId(), getArgs(1).toLowerCase(), radius);
				} else {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Tab auto-completion
	 *
	 * @return results Set
	 */
	@Override
	public Set<String> tabComplete() {
		// Show for console?
		if (!isAllowConsole() && fromConsole()) {
			return new HashSet<>();
		}

		Set<String> completions = new TreeSet<>();

		// Check if tabcomplete is enabled in the config first.
		boolean tabComplete = (boolean) Main.instance.getConfigHandler().getConfigValue("tabcomplete", null, "boolean");
		if (tabComplete) {

			// Check if sender has permission.
			if (Permission()) {

				// Handle different results for different number of args passed.
				if (getArgs().length == 1) {
					// /spawner <cmd>

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(0), getPossibilities(), completions);

				} else if (getArgs().length == 2 && isRightCommand()) {
					// /spawner <cmd> <entity>

					if (getSender().hasPermission("spawner.remove")) {

						Set<String> interimResults = new HashSet<>();

						// Loop through entities to add to tab completion list.
						for (EntityType entity : EntityType.values()) {
							if (entity != null && entity.getName() != null) {
								String lowerEntityName = entity.getName().toLowerCase();
								interimResults.add(lowerEntityName);
							}
						}

						// Check for partial matches.
						StringUtil.copyPartialMatches(getArgs(1), interimResults, completions);
					}

				} else if (getArgs().length == 3 && isRightCommand()) {
					// /spawner <cmd> <entity> <radius>

					if (getSender().hasPermission("spawner.remove")) {
						completions.add("<radius>");
					}
				}
			}
		}
		return completions;
	}
}
