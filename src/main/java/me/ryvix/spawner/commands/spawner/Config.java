/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands.spawner;

import me.ryvix.spawner.ConfigUpdater;
import me.ryvix.spawner.Main;
import me.ryvix.spawner.commands.Subcommand;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.StringUtils;
import org.bukkit.util.StringUtil;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Config extends Subcommand {

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public Config(CommandSender sender, String[] args, boolean autocomplete) {
		super(sender, args, autocomplete);
	}

	/**
	 * Set default variables for this command.
	 */
	@Override
	public void setDefaults() {
		// Allow allowConsole to run this command?
		setAllowConsole(true);

		// Valid number of args array.
		addNumArgs(1);
		addNumArgs(2);
		addNumArgs(3);
		addNumArgs(4);
		addNumArgs(5);

		// Valid permissions array.
		addPermission("spawner.config");

		// Add commands to tab completion list.
		addPossibility("config");

		// Set cooldown from config.
		setCooldown(Main.instance.getConfigHandler().getCommands().getInt("commands.config.cooldown"));

		// Set cost from config.
		setCost(Main.instance.getConfigHandler().getCommands().getDouble("commands.config.cost"));
	}

	/**
	 * Command logic
	 *
	 * @return boolean false if command was not run, true if it ran successfully
	 */
	@Override
	public boolean Command() {
		boolean success = false;
		String topic = "config";

		if (getArgs().length == 2) {
			// Set usage topic for subcommands.
			topic = getArgs(1);

		} else if (getArgs().length == 3) {
			if (getArgs(1).equalsIgnoreCase("update")) {
				// /spawner config update <version>

				new ConfigUpdater(getArgs(2));
				success = true;
			}

		} else if (getArgs().length == 4) {
			if (getArgs(1).equalsIgnoreCase("get")) {
				// /spawner config get <config> <key>

				// TODO: validate options
				switch (getArgs(2)) {
					case "config":
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getConfig().getString(getArgs(3)));
						success = true;
						break;
					case "commands":
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getConfigHandler().getCommands().getString(getArgs(3)));
						success = true;
						break;
					case "entities":
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getConfigHandler().getEntities().getString(getArgs(3)));
						success = true;
						break;
					case "language":
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getConfigHandler().getLanguage().getString(getArgs(3)));
						success = true;
						break;
				}
			}

		} else if (getArgs().length == 5) {
			if (getArgs(1).equalsIgnoreCase("set")) {
				// /spawner config set <config> <key> <value>

				// TODO: validate options
				switch (getArgs(2)) {
					case "config":
						if (getArgs(4).equals("true") || getArgs(4).equals("false")) {
							Main.instance.getConfig().set(getArgs(3), Boolean.parseBoolean(getArgs(4)));
						} else if (StringUtils.isNumeric(getArgs(4))) {
							Main.instance.getConfig().set(getArgs(3), Integer.parseInt(getArgs(4)));
						} else {
							Main.instance.getConfig().set(getArgs(3), getArgs(4));
						}
						Main.instance.saveConfig();
						success = true;
						break;
					case "commands":
						if (getArgs(4).equals("true") || getArgs(4).equals("false")) {
							Main.instance.getConfigHandler().getCommands().set(getArgs(3), Boolean.parseBoolean(getArgs(4)));
						} else if (StringUtils.isNumeric(getArgs(4))) {
							Main.instance.getConfigHandler().getCommands().set(getArgs(3), Integer.parseInt(getArgs(4)));
						} else {
							Main.instance.getConfigHandler().getCommands().set(getArgs(3), getArgs(4));
						}
						Main.instance.getConfigHandler().getCommands().save();
						success = true;
						break;
					case "entities":
						if (getArgs(4).equals("true") || getArgs(4).equals("false")) {
							Main.instance.getConfigHandler().getEntities().set(getArgs(3), Boolean.parseBoolean(getArgs(4)));
						} else if (StringUtils.isNumeric(getArgs(4))) {
							Main.instance.getConfigHandler().getEntities().set(getArgs(3), Integer.parseInt(getArgs(4)));
						} else {
							Main.instance.getConfigHandler().getEntities().set(getArgs(3), getArgs(4));
						}
						Main.instance.getConfigHandler().getEntities().save();
						success = true;
						break;
					case "language":
						if (getArgs(4).equals("true") || getArgs(4).equals("false")) {
							Main.instance.getConfigHandler().getLanguage().set(getArgs(3), Boolean.parseBoolean(getArgs(4)));
						} else if (StringUtils.isNumeric(getArgs(4))) {
							Main.instance.getConfigHandler().getLanguage().set(getArgs(3), Integer.parseInt(getArgs(4)));
						} else {
							Main.instance.getConfigHandler().getLanguage().set(getArgs(3), getArgs(4));
						}
						Main.instance.getConfigHandler().getLanguage().save();
						success = true;
						break;
				}
			}
		}

		if (success) {
			Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("Success"));
			return true;

		} else {
			Main.instance.getLangHandler().sendMessage(getSender(), getUsage(topic));
		}

		return false;
	}

	/**
	 * Tab auto-completion
	 */
	@Override
	public Set<String> tabComplete() {

		Set<String> completions = new TreeSet<>();

		// Check if tabcomplete is enabled in the config first.
		boolean tabComplete = (boolean) Main.instance.getConfigHandler().getConfigValue("tabcomplete", null, "boolean");
		if (tabComplete) {

			// Check if sender has permission.
			if (Permission()) {

				// Handle different results for different number of args passed.
				if (getArgs().length == 1) {
					// /spawner <cmd>

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(0), getPossibilities(), completions);

				} else if (getArgs().length == 2 && isRightCommand()) {
					// /spawner <cmd> <operation>

					Set<String> interimResults = new HashSet<>();

					interimResults.add("get");
					interimResults.add("set");
					interimResults.add("update");

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(1), interimResults, completions);

				} else if (getArgs().length == 3 && isRightCommand()) {
					// /spawner <cmd> <operation> <config>

					if (getArgs(1).equals("update")) {
						Set<String> interimResults = new HashSet<>();

						// Scan data folder for subfolders which should be the old versions and use those for autocompletion.
						try (DirectoryStream<Path> stream = Files.newDirectoryStream(Main.instance.getDataFolder().toPath())) {
							for (Path entry : stream) {
								if (entry.toFile().isDirectory()) {
									interimResults.add(entry.getFileName().toString());
								}
							}
						} catch (IOException e) {
							e.printStackTrace();
						}

						// Check for partial matches.
						StringUtil.copyPartialMatches(getArgs(2), interimResults, completions);

					} else if (getArgs(1).equalsIgnoreCase("get") || getArgs(1).equalsIgnoreCase("set")) {
						Set<String> interimResults = new HashSet<>();

						interimResults.add("config");
						interimResults.add("commands");
						interimResults.add("entities");
						interimResults.add("language");

						// Check for partial matches.
						StringUtil.copyPartialMatches(getArgs(2), interimResults, completions);
					}

				} else if (getArgs().length == 4 && isRightCommand()) {
					// /spawner <cmd> <operation> <config> <key>

					if (getArgs(1).equalsIgnoreCase("get") || getArgs(1).equalsIgnoreCase("set")) {
						Set<String> interimResults = new HashSet<>();

						// Check which config they want and display keys from it.
						switch (getArgs(2)) {
							case "config":
								interimResults.addAll(Main.instance.getConfig().getKeys(false));

								// Don't show version since it shouldn't be changed.
								if (getArgs(1).equalsIgnoreCase("set")) {
									interimResults.remove("version");
								}
								break;
							case "commands":
								interimResults.addAll(Main.instance.getConfigHandler().getCommands().getKeys(false));
								break;
							case "entities":
								interimResults.addAll(Main.instance.getConfigHandler().getEntities().getKeys(false));
								break;
							case "language":
								interimResults.addAll(Main.instance.getConfigHandler().getLanguage().getKeys(false));
								break;
						}

						// Check for partial matches.
						StringUtil.copyPartialMatches(getArgs(3), interimResults, completions);
					}

				}
			}
		}
		return completions;
	}

	/**
	 * Get command usage info.
	 *
	 * @return String Command usage info.
	 */
	@Override
	public String getUsage(String topic) {
		String usage;
		switch (topic) {
			case "set":
				usage = "/spawner config set <key> <value>";
				break;
			case "get":
				usage = "/spawner config get <key>";
				break;
			case "update":
				usage = "/spawner config update <version>";
				break;
			case "config":
			default:
				usage = "/spawner config <set|get|update>";
				break;
		}
		return usage;
	}
}
