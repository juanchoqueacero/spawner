/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands.spawner;

import me.ryvix.spawner.Main;
import me.ryvix.spawner.SpawnerFunctions;
import me.ryvix.spawner.commands.Subcommand;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.StringUtil;

import java.util.HashSet;
import java.util.TreeSet;

public class Set extends Subcommand {

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public Set(CommandSender sender, String[] args, boolean autocomplete) {
		super(sender, args, autocomplete);
	}

	/**
	 * Set default variables for this command.
	 */
	@Override
	public void setDefaults() {
		// Allow allowConsole to run this command?
		setAllowConsole(false);

		// Valid number of args array.
		addNumArgs(2);

		// Valid permissions array.
		addPermission("spawner.set.all");

		// Loop through entities to add permissions.
		for (EntityType entity : EntityType.values()) {
			if (entity != null && entity.getName() != null) {
				String lowerEntityName = entity.getName().toLowerCase();

				// Add permissions.
				addPermission("spawner.set." + lowerEntityName);
			}
		}

		// Add commands to tab completion list.
		addPossibility("set");

		// Set cooldown from config.
		setCooldown(Main.instance.getConfigHandler().getCommands().getInt("commands.set.cooldown"));

		// Set cost from config.
		// entities.yml > commands.yml > config.yml
		double globalCost = (double) Main.instance.getConfigHandler().getConfigValue("set_cost", "double");
		if (globalCost > 0) {
			setCost(globalCost);
		}

		double commandsCost = Main.instance.getConfigHandler().getCommands().getDouble("commands.set.cost");
		if (commandsCost > 0) {
			setCost(commandsCost);
		}
	}

	/**
	 * Command logic
	 *
	 * @return boolean false if command was not run, true if it ran successfully
	 */
	@Override
	public boolean Command() {
		if (getArgs().length == 2) {

			// Check if entity name is valid.
			if (!SpawnerFunctions.isValidEntityName(getArgs(1))) {
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
				return false;
			}

			// Get spawner name from aliases.
			String spawnerName = SpawnerFunctions.convertAlias(getArgs(1)).toLowerCase();

			// Check entities.yml for cost override.
			double entitiesCost = Main.instance.getConfigHandler().getEntities().getDouble("entities." + spawnerName + ".set_cost");
			if (entitiesCost > 0) {
				setCost(entitiesCost);
			}

			// Check permissions.
			if (!getSender().hasPermission("spawner.set.all") && !getSender().hasPermission("spawner.set." + spawnerName)) {
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
				return false;
			}

			Player player = (Player) getSender();
			Block target = SpawnerFunctions.findSpawnerBlock(player.getUniqueId(), 20);

			if (target != null && target.getType() == Material.SPAWNER) {
				// Set type of spawner player is targeting.

				// Check permissions.
				if (!getSender().hasPermission("spawner.change.all") && !getSender().hasPermission("spawner.change." + spawnerName)) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
					return false;
				}

				// Make a new spawner from the given entity name and set the NBT from it onto the target block.
				if (!Main.instance.getNmsHandler().setSpawnerBlockNBT(SpawnerFunctions.makeSpawner(spawnerName), target)) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}

				// Send a message about it being changed.
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("SpawnerChangedTo", getArgs(1)));
				return true;

			} else {
				// Otherwise set type of spawner in players hand.

				PlayerInventory playerInv = player.getInventory();
				ItemStack itemInMainHand = playerInv.getItemInMainHand();
				ItemStack itemInOffHand = playerInv.getItemInOffHand();

				// If the player is holding a spawner.
				if (itemInMainHand.getType() != Material.SPAWNER && itemInOffHand.getType() != Material.SPAWNER) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NotPossible"));
					return false;
				}

				// Get a new spawner.
				me.ryvix.spawner.Spawner newSpawner;
				if (itemInMainHand.getType() == Material.SPAWNER) {
					newSpawner = SpawnerFunctions.makeSpawner(spawnerName, itemInMainHand.getAmount());
				} else {
					newSpawner = SpawnerFunctions.makeSpawner(spawnerName, itemInOffHand.getAmount());
				}

				// Check permissions.
				if (!getSender().hasPermission("spawner.change.all") && !getSender().hasPermission("spawner.change." + newSpawner.getEntityName().toLowerCase())) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
					return false;
				}

				// Get spawner type from given name.
				EntityType spawnerType = SpawnerFunctions.getSpawnerType(getArgs(1));
				if (spawnerType == null) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}

				// Set item in player's hand.
				if (itemInMainHand.getType() == Material.SPAWNER) {
					playerInv.setItemInMainHand(newSpawner);
				} else if (itemInOffHand.getType() == Material.SPAWNER) {
					playerInv.setItemInOffHand(newSpawner);
				}

				// Send a message about it being changed.
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("SpawnerChangedTo", spawnerName));

				return true;
			}

		}

		return false;
	}

	@Override
	public boolean hasEnoughCurrency() {
		if (Main.instance.enableEconomy()) {
			Economy econ = Main.instance.getEcon();
			if (econ != null) {
				String spawnerName = SpawnerFunctions.convertAlias(getArgs(1)).toLowerCase();

				// Check entities.yml for cost override.
				double entitiesCost = Main.instance.getConfigHandler().getEntities().getDouble("entities." + spawnerName + ".set_cost");
				if (entitiesCost > 0) {
					setCost(entitiesCost);
				}

				return econ.has((OfflinePlayer) getSender(), getCost());
			} else {
				// If economy is enabled but null then all actions should fail.
				return false;
			}
		}
		return true;
	}

	/**
	 * Tab auto-completion
	 */
	@Override
	public java.util.Set<String> tabComplete() {
		// Show for console?
		if (!isAllowConsole() && fromConsole()) {
			return new HashSet<>();
		}

		java.util.Set<String> completions = new TreeSet<>();

		// Check if tabcomplete is enabled in the config first.
		boolean tabComplete = (boolean) Main.instance.getConfigHandler().getConfigValue("tabcomplete", null, "boolean");
		if (tabComplete) {

			// Check if sender has permission.
			if (Permission()) {

				// Handle different results for different number of args passed.
				if (getArgs().length == 1) {
					// /spawner <cmd>

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(0), getPossibilities(), completions);

				} else if (getArgs().length == 2 && isRightCommand()) {
					// /spawner <cmd> <entity>

					java.util.Set<String> interimResults = new HashSet<>();

					// Loop through entities to add to tab completion list.
					for (EntityType entity : EntityType.values()) {
						if (entity != null && entity.getName() != null) {
							String lowerEntityName = entity.getName().toLowerCase();
							if (getSender().hasPermission("spawner.set.all") || getSender().hasPermission("spawner.set." + lowerEntityName)) {
								interimResults.add(lowerEntityName);
							}
						}
					}

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(1), interimResults, completions);
				}
			}
		}
		return completions;
	}
}
