/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands.spawner;

import me.ryvix.spawner.Main;
import me.ryvix.spawner.SpawnerFunctions;
import me.ryvix.spawner.commands.Subcommand;
import net.milkbowl.vault.economy.Economy;
import org.apache.commons.lang.StringUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.StringUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class Give extends Subcommand {

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public Give(CommandSender sender, String[] args, boolean autocomplete) {
		super(sender, args, autocomplete);
	}

	/**
	 * Set default variables for this command.
	 */
	@Override
	public void setDefaults() {
		// Allow allowConsole to run this command?
		setAllowConsole(true);

		// Valid number of args array.
		addNumArgs(2);
		addNumArgs(3);
		addNumArgs(4);

		// Valid permissions array.
		addPermission("spawner.give.all");
		addPermission("spawner.giveothers.all");

		// Loop through entities to add permissions.
		for (EntityType entity : EntityType.values()) {
			if (entity != null && entity.getName() != null) {
				String lowerEntityName = entity.getName().toLowerCase();

				// Add permissions.
				addPermission("spawner.give." + lowerEntityName);
			}
		}

		// Add initial command arg.
		addPossibility("give");

		// Set cooldown from config.
		setCooldown(Main.instance.getConfigHandler().getCommands().getInt("commands.give.cooldown"));

		// Set cost from config.
		// entities.yml > commands.yml > config.yml
		double globalCost = (double) Main.instance.getConfigHandler().getConfigValue("give_cost", "double");
		if (globalCost > 0) {
			setCost(globalCost);
		}

		double commandsCost = Main.instance.getConfigHandler().getCommands().getDouble("commands.give.cost");
		if (commandsCost > 0) {
			setCost(commandsCost);
		}
	}

	/**
	 * Command logic
	 *
	 * @return boolean false if command was not run, true if it ran successfully
	 */
	@Override
	public boolean Command() {
		String spawnerName = SpawnerFunctions.convertAlias(getArgs(1)).toLowerCase();

		if (getArgs().length == 2) {
			// /spawner <cmd> <entity>

			if (getSender().hasPermission("spawner.give.all") || getSender().hasPermission("spawner.give." + spawnerName)) {
				Player player = (Player) getSender();

				if (SpawnerFunctions.isValidEntityName(spawnerName)) {

					giveSpawner(player.getUniqueId(), spawnerName, 1);

					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("GivenSpawner", spawnerName));
					return true;

				} else {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}

			} else {
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
				return false;
			}

		} else if (getArgs().length == 3) {

			// amount parameter for give to self
			if (StringUtils.isNumeric(getArgs(2))) {
				// /spawner give <entity> <amount>

				int amount = Integer.parseInt(getArgs(2));

				// don't let them give more than 127 spawners or an NPE will occur
				if (amount > 127) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
					return false;
				}

				Player player = (Player) getSender();

				if (SpawnerFunctions.isValidEntityName(spawnerName)) {

					giveSpawner(player.getUniqueId(), spawnerName, amount);

					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("GivenSpawner", spawnerName));
					return true;

				} else {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}

			} else if (getSender().hasPermission("spawner.giveothers.all") || getSender().hasPermission("spawner.giveothers." + spawnerName)) {
				// give a spawner to others
				// /spawner give <entity> <player>

				if (SpawnerFunctions.isValidEntityName(spawnerName)) {

					// get a new spawner
					me.ryvix.spawner.Spawner newSpawner = SpawnerFunctions.makeSpawner(spawnerName);
					Player targetPlayer = Main.instance.getServer().getPlayer(getArgs(2));
					if (targetPlayer != null) {

						PlayerInventory inventory = targetPlayer.getInventory();
						int invSlot = inventory.firstEmpty();

						// drop spawner at player location or add it to their inv if they have space
						if (invSlot == -1) {

							// if target player is online drop it at their feet and tell them
							targetPlayer.getWorld().dropItem(targetPlayer.getLocation().add(0, 1, 0), newSpawner);
							Main.instance.getLangHandler().sendMessage(targetPlayer.getUniqueId(), Main.instance.getLangHandler().getText("SpawnerDropped", spawnerName));
							return true;

						} else {

							inventory.setItem(invSlot, newSpawner);

							// make sure to show the player the item
							targetPlayer.updateInventory();

							if (targetPlayer != null) {
								Main.instance.getLangHandler().sendMessage(targetPlayer.getUniqueId(), Main.instance.getLangHandler().getText("GivenSpawner", spawnerName));
							} else {
								Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NotDeliveredOffline", getArgs(2)));
								return false;
							}

							String[] vars = new String[2];
							vars[0] = spawnerName;
							vars[1] = targetPlayer.getName();
							Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("YouGaveSpawner", vars));
						}

						return true;

					} else {
						// tell sender the target didn't get the spawner
						Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NotDeliveredOffline", getArgs(2)));
						return false;
					}

				} else {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
					return false;
				}

			} else {
				Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
				return false;
			}

		} else if (getArgs().length == 4) {
			// give a spawner to others

			// amount parameter for give to others
			if (StringUtils.isNumeric(getArgs(3))) {

				int amount = Integer.parseInt(getArgs(3));

				// don't let them give more than 127 spawners or an NPE will occur
				if (amount > 127) {
					Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NoPermission"));
					return false;
				}

				if (getSender().hasPermission("spawner.giveothers.all") || getSender().hasPermission("spawner.giveothers." + spawnerName)) {
					// give a spawner
					// /spawner give <entity> <player> <amount>

					// amount parameter for give to self
					if (StringUtils.isNumeric(getArgs(3))) {

						if (SpawnerFunctions.isValidEntityName(spawnerName)) {

							// get a new spawner
							me.ryvix.spawner.Spawner newSpawner = SpawnerFunctions.makeSpawner(spawnerName, amount);

							// set name
							Player targetPlayer = Main.instance.getServer().getPlayer(getArgs(2));
							if (targetPlayer != null) {

								PlayerInventory inventory = targetPlayer.getInventory();
								int invSlot = inventory.firstEmpty();

								// drop spawner at player location or add it to their inv if they have space
								if (invSlot == -1) {

									// if target player is online drop it at their feet and tell them
									targetPlayer.getWorld().dropItem(targetPlayer.getLocation().add(0, 1, 0), newSpawner);
									Main.instance.getLangHandler().sendMessage(targetPlayer, Main.instance.getLangHandler().getText("SpawnerDropped", spawnerName));
									return true;

								} else {

									inventory.setItem(invSlot, newSpawner);

									Main.instance.getLangHandler().sendMessage(targetPlayer, Main.instance.getLangHandler().getText("GivenSpawner", spawnerName));

									// make sure to show the player the item
									targetPlayer.updateInventory();

									String[] vars = new String[2];
									vars[0] = spawnerName;
									vars[1] = targetPlayer.getName();
									Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("YouGaveSpawner", vars));
								}

								return true;

							} else {
								// tell sender the target didn't get the spawner
								Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("NotDeliveredOffline", getArgs(2)));
								return false;
							}

						} else {
							Main.instance.getLangHandler().sendMessage(getSender(), Main.instance.getLangHandler().getText("InvalidSpawner"));
							return false;
						}

					}
				}
			}
		}

		return false;
	}

	@Override
	public boolean hasEnoughCurrency() {
		if (Main.instance.enableEconomy()) {
			Economy econ = Main.instance.getEcon();
			if (econ != null) {
				String spawnerName = SpawnerFunctions.convertAlias(getArgs(1)).toLowerCase();

				// Check entities.yml for cost override.
				double entitiesCost = Main.instance.getConfigHandler().getEntities().getDouble("entities." + spawnerName + ".give_cost");
				if (entitiesCost > 0) {

					if (getArgs().length == 3 && StringUtils.isNumeric(getArgs(2))) {
						// /spawner give <entity> <amount>
						// amount parameter for give to self
						int amount = Integer.parseInt(getArgs(2));

						setCost(entitiesCost * amount);

					} else if (getArgs().length == 4 && StringUtils.isNumeric(getArgs(3))) {
						// /spawner give <entity> <player> <amount>
						// give a spawner to others

						int amount = Integer.parseInt(getArgs(3));

						setCost(entitiesCost * amount);

					} else {
						setCost(entitiesCost);
					}
				}

				return econ.has((OfflinePlayer) getSender(), getCost());
			} else {
				// If economy is enabled but null then all actions should fail.
				return false;
			}
		}
		return true;
	}

	/**
	 * Give a player a spawner.
	 *
	 * @param playerUUID  UUID
	 * @param spawnerName String
	 */
	private void giveSpawner(UUID playerUUID, String spawnerName, int amount) {
		// get a new spawner
		me.ryvix.spawner.Spawner newSpawner = SpawnerFunctions.makeSpawner(spawnerName, amount);

		Player player = Main.instance.getServer().getPlayer(playerUUID);

		// drop spawner at player location or add it to their inv if they have space
		PlayerInventory inventory = player.getInventory();

		if (inventory.firstEmpty() == -1) {
			player.getWorld().dropItem(player.getLocation().add(0, 1, 0), newSpawner);
		} else {
			int invSlot = inventory.firstEmpty();
			inventory.setItem(invSlot, newSpawner);

			// make sure to show the player the item
			player.updateInventory();
		}
	}

	/**
	 * Tab auto-completion
	 */
	@Override
	public Set<String> tabComplete() {

		Set<String> completions = new TreeSet<>();

		// Check if tabcomplete is enabled in the config first.
		boolean tabComplete = (boolean) Main.instance.getConfigHandler().getConfigValue("tabcomplete", null, "boolean");
		if (tabComplete) {

			// Check if sender has permission.
			if (Permission()) {

				// Handle different results for different number of args passed.
				if (getArgs().length == 1) {
					// /spawner <cmd>

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(0), getPossibilities(), completions);

				} else if (getArgs().length == 2 && isRightCommand()) {
					// /spawner <cmd> <entity>

					Set<String> interimResults = new HashSet<>();

					// Loop through entities to add to tab completion list.
					for (EntityType entity : EntityType.values()) {
						if (entity != null && entity.getName() != null) {
							String lowerEntityName = entity.getName().toLowerCase();
							if (getSender().hasPermission("spawner.give." + lowerEntityName)) {
								interimResults.add(lowerEntityName);
							}
						}
					}

					// Check for partial matches.
					StringUtil.copyPartialMatches(getArgs(1), interimResults, completions);

				} else if (getArgs().length == 3 && isRightCommand()) {
					// /spawner <cmd> <entity> <player>

					if (getSender().hasPermission("spawner.give.others")) {

						Set<String> interimResults = new HashSet<>();

						// Loop through players to add to tab completion list.
						for (Player player : Main.instance.getServer().getOnlinePlayers()) {
							if (player != null && player.getName() != null) {
								interimResults.add(player.getName());
							}
						}

						// Check for partial matches.
						StringUtil.copyPartialMatches(getArgs(2), interimResults, completions);
					}
				}
			}
		}
		return completions;
	}

}
