/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner.commands.spawner;

import me.ryvix.spawner.Main;
import me.ryvix.spawner.SpawnerFunctions;
import me.ryvix.spawner.commands.Subcommand;
import org.bukkit.command.CommandSender;

import java.util.Set;

public class List extends Subcommand {

	/**
	 * Constructor
	 *
	 * @param sender       CommandSender
	 * @param args         String[]
	 * @param autocomplete boolean
	 */
	public List(CommandSender sender, String[] args, boolean autocomplete) {
		super(sender, args, autocomplete);
	}

	/**
	 * Set default variables for this command.
	 */
	@Override
	public void setDefaults() {
		// Allow allowConsole to run this command?
		setAllowConsole(true);

		// Valid number of args array.
		addNumArgs(1);

		// Valid permissions array.
		addPermission("spawner.list");

		// Add commands to tab completion list.
		addPossibility("list");

		// Set cooldown from config.
		setCooldown(Main.instance.getConfigHandler().getCommands().getInt("commands.list.cooldown"));

		// Set cost from config.
		setCost(Main.instance.getConfigHandler().getCommands().getDouble("commands.list.cost"));
	}

	/**
	 * Command logic
	 *
	 * @return boolean false if command was not run, true if it ran successfully
	 */
	@Override
	public boolean Command() {
		if (getArgs().length == 1) {

			// spawner help
			// Get the entities.
			Set<String> entities = SpawnerFunctions.getEntities(true, true);

			// Send the list of entities.
			// For performance reasons send the entire list here instead of checking for the spawner.enabled.bypass permission for every one.
			Main.instance.getLangHandler().sendMessage(getSender(), String.join(", ", entities));

			return true;
		}

		return false;
	}
}
