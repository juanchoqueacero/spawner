/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;
import java.util.Set;

// Build against Spigot not Bukkit or you will get an error here.

public class SpawnerEvents implements Listener {

	/**
	 * When a spawner is broken.
	 *
	 * @param event Block break event
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	private void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Material material = block.getType();

		if (material == Material.SPAWNER) {

			Player player = event.getPlayer();

			// Get spawner block.
			CreatureSpawner csBlock = (CreatureSpawner) event.getBlock().getState();

			// Get spawner type.
			EntityType spawnerType = csBlock.getSpawnedType();

			// Check if EntityType name is invalid.
			String spawnerName = spawnerType.getKey().getKey();

			// if they can't mine it just let them break it normally
			boolean canMine = false;
			if (player.hasPermission("spawner.mine.all") || player.hasPermission("spawner.mine." + spawnerName)) {
				canMine = true;
			}

			// if they can't break then cancel the event
			boolean canBreak = false;
			if (player.hasPermission("spawner.break.all") || player.hasPermission("spawner.break." + spawnerName)) {
				canBreak = true;
			}

			// check if they can mine
			if (!canMine) {

				// check if they can break
				if (!canBreak) {
					// don't let them break it
					event.setCancelled(true);
				}

				// just let them break it
				return;
			}

			// apply luck
			if (!SpawnerFunctions.chance(spawnerName, "luck")) {
				// don't drop anything at all
				return;
			}

			PlayerInventory playerInv = player.getInventory();

			boolean break_into_inventory = (boolean) Main.instance.getConfigHandler().getConfigValue("break_into_inventory", spawnerName, "boolean");
			boolean prevent_break_if_inventory_full = (boolean) Main.instance.getConfigHandler().getConfigValue("prevent_break_if_inventory_full", spawnerName, "boolean");

			// prevent break if inv is full and prevent_break_if_inventory_full option is true
			if (break_into_inventory && playerInv.firstEmpty() == -1 && prevent_break_if_inventory_full) {
				Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("InventoryFull"));
				event.setCancelled(true);
				return;
			}

			// check for drops
			int amount;
			Material type;
			boolean silk;
			boolean doDrop = true;

			// Get drops from config.
			ConfigurationSection drops = (ConfigurationSection) Main.instance.getConfigHandler().getConfigValue("drops", spawnerName, "ConfigurationSection");
			if (drops != null) {

				// Make sure XP orbs can still drop
				Set<String> dropKeys = drops.getKeys(false);
				boolean hasXP = false;
				if (dropKeys.contains("EXPERIENCE_ORB")) {
					hasXP = true;
				}

				// Loop through each drop.
				for (String drop : dropKeys) {

					// Get that drop section.
					ConfigurationSection dropSection = (ConfigurationSection) Main.instance.getConfigHandler().getConfigValue("drops." + drop, spawnerName, "ConfigurationSection");
					if (dropSection != null && dropSection.contains("amount") && dropSection.contains("silk")) {
						// Init. variables.
						type = Material.matchMaterial(drop);

						boolean isXP = drop.equalsIgnoreCase("EXPERIENCE_ORB");

						if (type == null && !isXP) {
							Main.instance.getLogger().info("Invalid drop in config: " + drop);
							continue;
						}

						amount = dropSection.getInt("amount");
						silk = dropSection.getBoolean("silk");

						// Handle silk touch.
						if ((silk && playerInv.getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH))
								|| (silk && playerInv.getItemInOffHand().containsEnchantment(Enchantment.SILK_TOUCH))
								|| (!silk && !playerInv.getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH))
								|| (!silk && !playerInv.getItemInOffHand().containsEnchantment(Enchantment.SILK_TOUCH))) {

							// don't drop spawner anymore
							doDrop = false;

							// handle experience orbs
							if (isXP) {
								// drop exp
								event.setExpToDrop(amount);
								continue;
							} else if (!hasXP) {
								// stop exp
								event.setExpToDrop(0);
							}

							// make fireworks!
							if (drop.equalsIgnoreCase("FIREWORK_ROCKET")) {
								final Location loc = block.getLocation();
								Random rand = new Random();
								for (int i = 1; i <= amount; ++i) {
									// add delays between them
									int delay = rand.nextInt(amount) * 20;
									new BukkitRunnable() {
										@Override
										public void run() {
											SpawnerFunctions.spawnRandomFirework(loc);
											loc.subtract(0.0D, 2.0D, 0.0D);
										}
									}.runTaskLater(Main.instance, delay);
								}

								// TODO: add an option to drop fireworks too
								continue;
							}

							// make an ItemStack
							ItemStack dropItem = new ItemStack(type);
							dropItem.setAmount(amount);

							if (!break_into_inventory || (playerInv.firstEmpty() == -1 && !prevent_break_if_inventory_full)) {
								// drop item
								csBlock.getWorld().dropItemNaturally(csBlock.getLocation(), dropItem);
							} else {
								// place into free inventory slot
								int invSlot = playerInv.firstEmpty();
								playerInv.setItem(invSlot, dropItem);

								// make sure to show the player the item
								player.updateInventory();
							}
						}
					}
				}
			}

			// if they are in creative or have silk touch and not holding a spawner
			if ((player.getGameMode() == GameMode.CREATIVE
					|| (player.hasPermission("spawner.nosilk.all")
					|| player.hasPermission("spawner.nosilk." + spawnerName)
					|| (playerInv.getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)
					|| playerInv.getItemInOffHand().containsEnchantment(Enchantment.SILK_TOUCH))))
					&& playerInv.getItemInMainHand().getType() != Material.SPAWNER) {

				// drop spawner
				if (doDrop) {

					// stop exp
					event.setExpToDrop(0);

					// get a new spawner
					Spawner dropSpawner = SpawnerFunctions.makeSpawner(spawnerName);

					// drop spawner
					if (!break_into_inventory || (playerInv.firstEmpty() == -1 && !prevent_break_if_inventory_full)) {
						// drop spawner
						csBlock.getWorld().dropItemNaturally(csBlock.getLocation(), dropSpawner);
					} else {
						// place into free inventory slot
						int invSlot = playerInv.firstEmpty();
						playerInv.setItem(invSlot, dropSpawner);

						// make sure to show the player the spawner
						player.updateInventory();
					}

				}
			}

		} else {
			RedstoneFunctions.redstoneCheck(block);
		}
	}

	/**
	 * When a spawner is placed.
	 *
	 * @param event Block place event
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	private void onBlockPlace(BlockPlaceEvent event) {

		if (event.getHand() == null) {
			// Player didn't use their hand for this event.
			return;
		}

		Block block = event.getBlock();
		Material material = block.getType();

		if (material == Material.SPAWNER) {
			Player player = event.getPlayer();

			// Get held spawner.
			Spawner spawner = null;
			PlayerInventory playerInv = player.getInventory();
			if (playerInv.getItemInMainHand().getType() == Material.SPAWNER) {
				spawner = new Spawner(playerInv.getItemInMainHand());
			} else if (playerInv.getItemInOffHand().getType() == Material.SPAWNER) {
				spawner = new Spawner(playerInv.getItemInOffHand());
			}

			// Held spawner not found.
			if (spawner == null) {
				return;
			}

			// if they can't place it cancel event
			if (!player.hasPermission("spawner.place.all") && !player.hasPermission("spawner.place." + spawner.getEntityName().toLowerCase())) {
				event.setCancelled(true);
				Main.instance.getLangHandler().sendMessage(event.getPlayer(), Main.instance.getLangHandler().getText("NoPermission", spawner.getFormattedEntityName()));
				return;
			}

			// set spawner type
			if (Main.instance.getNmsHandler().setSpawnerBlockNBT(spawner, block)) {
				Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("PlacedSpawner", spawner.getFormattedEntityName()));
			} else {
				event.setCancelled(true);
				Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NotPossible"));
			}

			// detect redstone power
			CreatureSpawner creatureSpawner = (CreatureSpawner) block.getState();

			boolean power_with_redstone = (boolean) Main.instance.getConfigHandler().getConfigValue("power_with_redstone", spawner.getSpawnerType().getKey().getKey(), "boolean");
			if (power_with_redstone) {

				// invert redstone power
				// TODO: check entity config for proper default value
				int on = 16;
				int off = 0;

				if (Main.instance.getInvertRedstone()) {
					on = 0;
					off = 16;
				}

				// TODO: use delay instead of range
				// TODO: move into Spawner or make a new SpawnerBlock class
				if (block.getBlockPower() > 0) {
					creatureSpawner.setRequiredPlayerRange(on);
				} else {
					creatureSpawner.setRequiredPlayerRange(off);
				}

				creatureSpawner.update();

			} else if (creatureSpawner.getRequiredPlayerRange() == 0) {
				// enable spawners that are off if they shouldn't be affected by redstone power
				creatureSpawner.setRequiredPlayerRange(16);
				creatureSpawner.update();
			}

		} else {
			RedstoneFunctions.redstoneCheck(block);
		}
	}

	/**
	 * When a spawner is exploded.
	 *
	 * @param event Entity explode event
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	private void onEntityExplode(EntityExplodeEvent event) {

		if (event.blockList().isEmpty()) {
			return;
		}

		// Check if the explosion hits any spawners.
		for (Block block : event.blockList()) {

			// If block was a spawner
			if (block.getType().equals(Material.SPAWNER)) {

				// Get spawner name
				EntityType spawnerType = SpawnerFunctions.getSpawner(block);
				String spawnerName = spawnerType.getKey().getKey();

				boolean protect_from_explosions = (boolean) Main.instance.getConfigHandler().getConfigValue("protect_from_explosions", spawnerName, "boolean");

				if (protect_from_explosions) {
					// protect_from_explosions
					event.setCancelled(true);
					return;

				} else {

					boolean drop_from_explosions = (boolean) Main.instance.getConfigHandler().getConfigValue("drop_from_explosions", spawnerName, "boolean");

					if (drop_from_explosions) {
						// drop_from_explosions

						// get a new spawner
						Spawner dropSpawner = SpawnerFunctions.makeSpawner(spawnerName);

						// drop item
						block.getWorld().dropItemNaturally(block.getLocation(), dropSpawner);
					}
				}
			}
		}
	}

	/**
	 * When a spawner is held.
	 *
	 * @param event PlayerItemHeldEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	private void onPlayerItemHeld(PlayerItemHeldEvent event) {

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
			return;
		}
		ItemStack itemStack = event.getPlayer().getInventory().getItem(event.getNewSlot());

		if (itemStack != null && itemStack.getType().equals(Material.SPAWNER)) {
			Spawner spawner = SpawnerFunctions.makeSpawner(itemStack);

			event.getPlayer().updateInventory();

			Main.instance.getLangHandler().sendMessage(event.getPlayer(), Main.instance.getLangHandler().getText("HoldingSpawner", spawner.getFormattedEntityName()));
		}
	}

	/**
	 * When a player interacts with a spawner.
	 *
	 * @param event PlayerInteractEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
	private void onPlayerInteract(PlayerInteractEvent event) {
		if (!event.hasBlock()) {
			return;
		}

		EquipmentSlot hand = event.getHand();

		if (hand == null) {
			// Player didn't use their hand for this event.
			return;
		}

		Block clicked = event.getClickedBlock();

		// Check if a mob spawner was clicked.
		if (clicked != null && clicked.getType().equals(Material.SPAWNER)) {
			ItemStack itemInHand = event.getItem();

			boolean isEgg = false;

			if (itemInHand != null) {
				Material eggType = itemInHand.getType();
				isEgg = eggType.toString().contains("SPAWN_EGG");
			}

			Player player = event.getPlayer();

			// Check permission for spawner eggs.
			if (isEgg) {
				// If the player is trying to change a spawner with a spawn egg.

				String eggId = SpawnerFunctions.getEntityNameFromSpawnEgg(itemInHand);
				Spawner spawner = SpawnerFunctions.makeSpawner(eggId);
				String spawnerName = spawner.getEntityName();

				if (spawnerName == null) {
					Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NotPossible"));
					event.setCancelled(true);
					return;
				}

				// If they don't have permission to use this spawn egg cancel the event.
				if (!player.hasPermission("spawner.eggs.all") && !player.hasPermission("spawner.eggs." + spawnerName.toLowerCase())) {
					Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NoPermission"));
					event.setCancelled(true);
					return;
				}

				CreatureSpawner csBlock = (CreatureSpawner) clicked.getState();
				EntityType spawnerType = csBlock.getSpawnedType();

				// Check if entity is spawnable and if not make it a pig by default.
				if (!spawnerType.isSpawnable()) {
					spawnerType = EntityType.PIG;
				}

				// If they don't have permission to change this type of spawner cancel the event.
				if (!player.hasPermission("spawner.change.all") && !player.hasPermission("spawner.change." + spawnerType.getKey().getKey())) {
					Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NoPermission"));
					event.setCancelled(true);
					return;
				}

				// Set spawner block NBT.
				Main.instance.getNmsHandler().setSpawnerBlockNBT(spawner, clicked);

				// Remove the egg from the player's inventory.
				itemInHand.setAmount(itemInHand.getAmount() - 1);
				if (hand == EquipmentSlot.HAND) {
					player.getInventory().setItemInMainHand(itemInHand);
				} else {
					player.getInventory().setItemInOffHand(itemInHand);
				}

				// Cancel the event or else the spawn egg will reset the NBT data.
				event.setCancelled(true);

				// Send the player a message.
				Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("SpawnerChangedTo", spawner.getName()));

			} else if (event.getAction() == Action.RIGHT_CLICK_BLOCK && hand == EquipmentSlot.OFF_HAND) {
				// TODO: The server triggers the event for both hands. Unfortunately I can't find a good way to make all items work when right clicking.

				// Allow right-clicking a spawner to get the name of it.
				if (player.hasPermission("spawner.get")) {

					CreatureSpawner csBlock = (CreatureSpawner) clicked.getState();

					// Get spawner type.
					EntityType spawnerType = csBlock.getSpawnedType();

					// Try to fix this: https://gitlab.com/rwr/spawner/-/issues/62
					if (spawnerType == null || spawnerType.getKey() == null) {
						Main.instance.getLogger().info("Null entity type or key from right-clicked spawner: " + clicked.toString());
					}

					Spawner spawner = SpawnerFunctions.makeSpawner(spawnerType.getKey().getKey());

					Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("SpawnerType", spawner.getName()));

					RedstoneFunctions.powerCheck(clicked);
				}
			}
		}
	}

	/**
	 * CreatureSpawnEvent for spawn frequency chance.
	 *
	 * @param event CreatureSpawnEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	private void onCreatureSpawn(CreatureSpawnEvent event) {
		if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER) {

			// get type of entity
			String entityName = event.getEntityType().getName();

			// apply spawn frequency chance
			boolean doSpawn = SpawnerFunctions.chance(entityName, "frequency");
			if (!doSpawn) {

				// stop creature from spawning
				event.setCancelled(true);
			}
		}
	}

	/**
	 * SpawnerSpawnEvent for FireworksRocketEntity and XPOrb with spawn frequency chance.
	 *
	 * @param event CreatureSpawnEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	private void onSpawnerSpawn(SpawnerSpawnEvent event) {
		boolean doSpawn = true;

		// get type of entity
		String entityName = event.getEntityType().getKey().getKey();

		Block spawnerBlock = event.getSpawner().getBlock();

		// detect redstone power
		boolean power_with_redstone = (boolean) Main.instance.getConfigHandler().getConfigValue("power_with_redstone", entityName, "boolean");
		if (power_with_redstone) {

			if ((Main.instance.getInvertRedstone() && spawnerBlock.getBlockPower() > 0) || (!Main.instance.getInvertRedstone() && spawnerBlock.getBlockPower() < 1)) {
				CreatureSpawner creatureSpawner = (CreatureSpawner) spawnerBlock.getState();
				creatureSpawner.setRequiredPlayerRange(0);
				creatureSpawner.update();
				doSpawn = false;
			}
		}

		RedstoneFunctions.powerCheck(spawnerBlock);

		if (doSpawn) {
			// apply spawn frequency chance
			doSpawn = SpawnerFunctions.chance(entityName, "frequency");
		}

		if (!doSpawn) {
			// stop creature from spawning
			event.setCancelled(true);
			return;
		}

		// handle specific spawner types
		switch (entityName) {
			case "firework_rocket":
				// make fireworks!
				SpawnerFunctions.spawnRandomFirework(event.getLocation());
				break;
			case "experience_orb":
				// make xp orbs drop xp worth something
				ExperienceOrb xpOrb = (ExperienceOrb) event.getEntity();
				Random random = new Random();
				xpOrb.setExperience(random.nextInt(10 - 1) + 1);
				break;
			case "giant":
				break;

			case "fireball":
			case "small_fireball":
			case "dragon_fireball":
				break;
			case "falling_block":
				break;
			case "wither_skull":
			default:
				break;
		}
	}

	/**
	 * Prevent anvil renaming of spawners.
	 *
	 * @param event InventoryClickEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	private void onInventoryClickEvent(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (player == null) {
			return;
		}

		ItemStack item = event.getCurrentItem();
		if (item == null) {
			return;
		}

		Material type = item.getType();
		if (type == null) {
			return;
		}

		if (!(event.getWhoClicked() instanceof Player)) {
			return;
		}

		if (!(event.getInventory() instanceof AnvilInventory)) {
			return;
		}

		if (event.getSlotType() != SlotType.RESULT) {
			return;
		}

		if (type == Material.SPAWNER && !player.hasPermission("spawner.anvil.spawners")) {
			Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NoPermission"));
			event.setCancelled(true);
			return;
		}

		if (type == Material.EGG && !player.hasPermission("spawner.anvil.eggs")) {
			Main.instance.getLangHandler().sendMessage(player, Main.instance.getLangHandler().getText("NoPermission"));
			event.setCancelled(true);
		}
	}
}