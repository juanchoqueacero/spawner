/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import me.ryvix.spawner.api.Config;
import me.ryvix.spawner.api.Language;
import me.ryvix.spawner.api.NMS;
import me.ryvix.spawner.commands.CommandSpawner;
import net.milkbowl.vault.economy.Economy;
import org.bstats.bukkit.MetricsLite;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public static Main instance;

	private String serverVersion;
	private String pluginVersion;

	private Configuration config;

	private Economy econ;

	private NMS nmsHandler;
	private Config configHandler;
	private Language langHandler;

	private boolean redstone;
	private boolean advanced_redstone;
	private int advanced_redstone_max_depth;
	private boolean invertRedstone;
	private boolean particles;

	@Override
	public void onEnable() {
		instance = this;

		// Validate this server version is supported
		if (!validateVersion()) {
			disablePlugin();
			return;
		}

		// Set server version
		String packageName = instance.getServer().getClass().getPackage().getName();
		setServerVersion(packageName.substring(packageName.lastIndexOf('.') + 1));

		// Set plugin version
		setPluginVersion(getDescription().getVersion());

		try {
			// NMS
			final Class<?> nmsClazz = Class.forName("me.ryvix.spawner.NMSHandler");
			// Check if we have a NMSHandler class at that location.
			// Make sure it actually implements NMS
			if (NMS.class.isAssignableFrom(nmsClazz)) {
				// Set our handler
				instance.setNmsHandler((NMS) nmsClazz.getConstructor().newInstance());
			}

			// Config
			final Class<?> configClazz = Class.forName("me.ryvix.spawner.ConfigHandler");
			// Check if we have a ConfigHandler class at that location.
			// Make sure it actually implements Config functions
			if (Config.class.isAssignableFrom(configClazz)) {
				// Set our handler
				instance.setConfigHandler((Config) configClazz.getConstructor().newInstance());
			}

			// Language
			final Class<?> langClazz = Class.forName("me.ryvix.spawner.LangHandler");
			// Check if we have a Language class at that location.
			// Make sure it actually implements Language functions
			if (Language.class.isAssignableFrom(langClazz)) {
				// Set our handler
				instance.setLangHandler((Language) langClazz.getConstructor().newInstance());
			}

		} catch (final Exception e) {
			e.printStackTrace();
			disablePlugin();
			return;
		}
		instance.getLogger().info("Loading support for " + getServerVersion());

		/*
		  Metrics

		  @link https://bstats.org/getting-started/include-metrics
		 */
		new MetricsLite(this, 162);

		// load files
		getConfigHandler().loadFiles();

		// Update files
		new ConfigUpdater();

		// Load economy support.
		if (enableEconomy() && setupEconomy()) {
			instance.getLogger().info("Vault found, loading economy support!");
		}

		// register events
		getServer().getPluginManager().registerEvents(new SpawnerEvents(), this);

		// register redstone events
		if (instance.getSpawnerConfig().getBoolean("power_with_redstone")) {
			setRedstone(true);
			setAdvancedRedstone(instance.getSpawnerConfig().getBoolean("advanced_redstone"));
			setAdvancedRedstoneMaxDepth(instance.getSpawnerConfig().getInt("advanced_redstone_max_depth"));
			setInvertRedstone(instance.getSpawnerConfig().getBoolean("invert_redstone"));
			setParticles(instance.getSpawnerConfig().getBoolean("redstone_particles"));
			getServer().getPluginManager().registerEvents(new RedstoneEvents(), this);
		}

		// spawner
		getCommand("spawner").setExecutor(new CommandSpawner());
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	@Override
	public void onDisable() {
		config = null;
		nmsHandler = null;
		configHandler = null;
		langHandler = null;
	}

	private boolean validateVersion() {

		// https://hub.spigotmc.org/nexus/content/repositories/snapshots/org/bukkit/bukkit/maven-metadata.xml
		String[] validVersions = {
				"1.16.4-R0.1-SNAPSHOT",
				"1.16.5-R0.1-SNAPSHOT"
		};

		// Do version number check.
		String currentVersion = this.getServer().getBukkitVersion();
		for (String version : validVersions) {
			if (version.equals(currentVersion)) {
				return true;
			}
		}

		return false;
	}

	private void disablePlugin() {
		instance.getLogger().severe("This plugin is not compatible with the version of your server.");
		instance.getLogger().severe("Please check for updates at " + instance.getDescription().getWebsite());
		instance.getLogger().severe("If an update is not available please request one if you are running Spigot 1.16.4 or newer.");
		instance.getServer().getPluginManager().disablePlugin(this);
	}

	/**
	 * Reload Spawner's files from disk.
	 */
	public void reloadFiles() {
		reloadConfig();
		getConfigHandler().reloadFiles();
		setRedstone(instance.getSpawnerConfig().getBoolean("power_with_redstone"));
		setAdvancedRedstone(instance.getSpawnerConfig().getBoolean("advanced_redstone"));
		setAdvancedRedstoneMaxDepth(instance.getSpawnerConfig().getInt("advanced_redstone_max_depth"));
		setInvertRedstone(instance.getSpawnerConfig().getBoolean("invert_redstone"));
		setParticles(instance.getSpawnerConfig().getBoolean("redstone_particles"));
	}

	public Configuration getSpawnerConfig() {
		return config;
	}

	public void setSpawnerConfig(Configuration cfg) {
		config = cfg;
	}

	public Config getConfigHandler() {
		return this.configHandler;
	}

	public void setConfigHandler(Config configHandler) {
		this.configHandler = configHandler;
	}

	public Language getLangHandler() {
		return this.langHandler;
	}

	public void setLangHandler(Language langHandler) {
		this.langHandler = langHandler;
	}

	public NMS getNmsHandler() {
		return this.nmsHandler;
	}

	public void setNmsHandler(NMS nmsHandler) {
		this.nmsHandler = nmsHandler;
	}

	public String getServerVersion() {
		return serverVersion;
	}

	public void setServerVersion(String ver) {
		serverVersion = ver;
	}

	public Economy getEcon() {
		return econ;
	}

	public void setEcon(Economy econ) {
		this.econ = econ;
	}

	public boolean enableEconomy() {
		return (boolean) Main.instance.getConfigHandler().getConfigValue("economy", "", "boolean");
	}

	public String getPluginVersion() {
		return pluginVersion;
	}

	public void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	public boolean getRedstone() {
		return redstone;
	}

	public void setRedstone(boolean redstone) {
		this.redstone = redstone;
	}

	public boolean getAdvancedRedstone() {
		return advanced_redstone;
	}

	public void setAdvancedRedstone(boolean advanced_redstone) {
		this.advanced_redstone = advanced_redstone;
	}

	public boolean getInvertRedstone() {
		return invertRedstone;
	}

	public void setInvertRedstone(boolean invertRedstone) {
		this.invertRedstone = invertRedstone;
	}

	public boolean getParticles() {
		return particles;
	}

	public void setParticles(boolean particles) {
		this.particles = particles;
	}

	public int getAdvancedRedstoneMaxDepth() {
		return advanced_redstone_max_depth;
	}

	public void setAdvancedRedstoneMaxDepth(int advanced_redstone_max_depth) {
		this.advanced_redstone_max_depth = advanced_redstone_max_depth;
	}
}
