/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2020 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class RedstoneFunctions {

	private static final Set<Vector> locations = new HashSet<>();
	private static int depth = 0;

	/**
	 * Check if a spawner should be powered by the given redstone block.
	 *
	 * @param block Block
	 */
	public static void redstoneCheck(final Block block) {
		depth = 0;
		redstoneCheck(block, null);
		locations.clear();
	}

	/**
	 * Check if a spawner should be powered by the given redstone block.
	 * Won't check from the given block face.
	 *
	 * @param block Block
	 * @param from  BlockFace
	 */
	public static void redstoneCheck(final Block block, BlockFace from) {

		if (!Main.instance.getRedstone()) {
			return;
		}

		boolean advanced = Main.instance.getAdvancedRedstone();
		int max = Main.instance.getAdvancedRedstoneMaxDepth();

		// Check each side of the event block for spawners
		BlockFace[] faces = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};
		for (BlockFace face : faces) {

			// Don't go back where we came from
			if (advanced && face.getOppositeFace().equals(from)) {
				continue;
			}

			// Get the block to check for power
			Block checkBlock = block.getRelative(face);

			// If advanced redstone detection is enabled and haven't reached max depth yet
			if (advanced && depth < max) {

				// Check if we've already seen this location
				Vector loc = new Vector(checkBlock.getX(), checkBlock.getY(), checkBlock.getZ());
				if (locations.contains(loc)) {
					continue;
				}

				// Save location as seen
				locations.add(loc);

				// Increase depth
				depth++;

				// Spawner was found so make sure it's enabled/disabled
				if (checkBlock.getType() == Material.SPAWNER) {
					powerCheck(checkBlock);
				}

				// Follow the power to the max
				if (checkBlock.getBlockPower() > 0) {
					redstoneCheck(checkBlock, face);
				}

			} else {
				// Spawner was found so make sure it's enabled/disabled
				if (checkBlock.getType() == Material.SPAWNER) {
					powerCheck(checkBlock);
				}
			}
		}
	}

	/**
	 * Check the given spawner block for power.
	 *
	 * @param spawnerBlock Block
	 */
	public static void powerCheck(final Block spawnerBlock) {

		final CreatureSpawner creatureSpawner = (CreatureSpawner) spawnerBlock.getState();
		boolean power_with_redstone = (boolean) Main.instance.getConfigHandler().getConfigValue("power_with_redstone", creatureSpawner.getSpawnedType().getKey().getKey(), "boolean");

		if (power_with_redstone) {

			// Schedule task or power will still be on during block break event.
			new BukkitRunnable() {

				@Override
				public void run() {

					int on = 16;
					int off = 0;

					if (Main.instance.getInvertRedstone()) {
						on = 0;
						off = 16;
					}

					if (spawnerBlock.getBlockPower() > 0) {
						particles(spawnerBlock, true);
						creatureSpawner.setRequiredPlayerRange(on);

					} else {
						particles(spawnerBlock, false);
						creatureSpawner.setRequiredPlayerRange(off);
					}

					creatureSpawner.update();

					// Check for surrounding spawners because they seem to transmit power a single block away.
					BlockFace[] faces = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};
					for (BlockFace face : faces) {
						Block checkBlock = spawnerBlock.getRelative(face);

						// Spawner was found so make sure it's enabled
						if (checkBlock.getType() == Material.SPAWNER) {
							CreatureSpawner creatureSpawner2 = (CreatureSpawner) checkBlock.getState();
							boolean power_with_redstone = (boolean) Main.instance.getConfigHandler().getConfigValue("power_with_redstone", creatureSpawner2.getSpawnedType().getKey().getKey(), "boolean");
							if (power_with_redstone) {
								if (checkBlock.getBlockPower() > 0) {
									particles(checkBlock, true);
									creatureSpawner2.setRequiredPlayerRange(on);

								} else {
									particles(checkBlock, false);
									creatureSpawner2.setRequiredPlayerRange(off);
								}

								creatureSpawner2.update();
							}
						}
					}
				}

			}.runTaskLater(Main.instance, 0);

		} else if (creatureSpawner.getRequiredPlayerRange() == 0) {
			// enable spawners that are off if they shouldn't be affected by redstone power
			creatureSpawner.setRequiredPlayerRange(16);
			creatureSpawner.update();
		}
	}

	/**
	 * Spawns particles to show if the spawner is powered on or off.
	 *
	 * @param spawnerBlock Block
	 * @param powered      boolean
	 */
	public static void particles(Block spawnerBlock, boolean powered) {
		// only show particles if they are enabled in the config
		if (!Main.instance.getParticles()) {
			return;
		}

		Location loc = spawnerBlock.getLocation().add(0.5, 0.5, 0.5);

		Color color;
		Color green = Color.fromRGB(0, 255, 0);
		Color red = Color.fromRGB(255, 0, 0);

		if (powered) {
			if (Main.instance.getInvertRedstone()) {
				color = red;
			} else {
				color = green;
			}

		} else {
			if (Main.instance.getInvertRedstone()) {
				color = green;
			} else {
				color = red;
			}
		}

		Particle.DustOptions dustOptions = new Particle.DustOptions(color, 4);
		spawnerBlock.getWorld().spawnParticle(Particle.REDSTONE, loc, 1, 0.22, 0.22, 0.22, dustOptions);
	}
}
